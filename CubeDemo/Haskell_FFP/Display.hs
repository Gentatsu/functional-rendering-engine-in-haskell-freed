module Display (idle, display, State(..), makeState) where
 
import Graphics.UI.GLUT hiding (Color, Cube, Texture)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Graphics.GLUtil
import Linear hiding (rotate, lookAt, perspective)

data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

display :: NumArrayIndices -> State -> DisplayCallback
display size state = do 
	clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
	matrixMode $= Projection
	loadIdentity
	--p <- toGLMatrix proj
	--multMatrix  p
	perspective 45 1 0.1 1500
	matrixMode $= Modelview 0
	loadIdentity
	preservingMatrix $ do
		--m <- toGLMatrix mv	
		--multMatrix m	
		lookAt (Vertex3 2 4 2) (Vertex3 0 0 0) (Vector3 0 1 0)
		preservingMatrix $ do
			rotate 45 (Vector3 0 0 1 :: Vector3 GLfloat)
			drawArrays Triangles 0 $ size `div` 8
	swapBuffers
	frames state $~! (+1)
	t0' <- get (t0 state)
	t <- get elapsedTime
	when (t - t0' >= 5000) $ do
		f <- get (frames state)
		let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		let fps = fromIntegral f / seconds
		putStrLn (show fps)
		t0 state $= t
		frames state $= 0

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  
getCamPos :: GLfloat -> (GLfloat, GLfloat)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360


toGLMatrix :: M44 GLfloat -> IO (GLmatrix GLfloat)
toGLMatrix (V4 (V4 a11 a12 a13 a14)
                 (V4 a21 a22 a23 a24)
                 (V4 a31 a32 a33 a34)
                 (V4 a41 a42 a43 a44))
    = newMatrix RowMajor [ realToFrac a11, realToFrac a12, realToFrac a13, realToFrac a14
                               , realToFrac a21, realToFrac a22, realToFrac a23, realToFrac a24
                               , realToFrac a31, realToFrac a32, realToFrac a33, realToFrac a34
                               , realToFrac a41, realToFrac a42, realToFrac a43, realToFrac a44
                               ]
					
