/*
 * GLUT Shapes Demo
 *
 * Written by Nigel Stewart November 2003
 *
 * This program is test harness for the sphere, cone
 * and torus shapes in GLUT.
 *
 * Spinning wireframe and smooth shaded shapes are
 * displayed until the ESC or q key is pressed.  The
 * number of geometry stacks and slices can be adjusted
 * using the + and - keys.
 */

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include "SOIL.h"

#include "glm/glm.hpp"
#include "glm/ext.hpp"
using namespace glm;
#include "objloader.hpp"
#include <vector>

std::vector<glm::vec3> vertices;
std::vector<glm::vec2> uvs;
std::vector<glm::vec3> normals;

GLuint tex;
/* GLUT callback Handlers */

GLuint LoadTexture( const char * filename, int width, int height )
{

  GLuint texture;


  unsigned char * data;

  FILE * file;

  file = fopen( filename, "rb" );

  if ( file == NULL ) return 0;
  data = (unsigned char *)malloc( width * height * 3 );
  //int size = fseek(file,);
  fread( data, width * height * 3, 1, file );
  fclose( file );

 for(int i = 0; i < width * height ; ++i)
{
   int index = i*3;
   unsigned char B,R;
   B = data[index];
   R = data[index+2];

   data[index] = R;
   data[index+2] = B;

}


glGenTextures( 1, &texture );
glBindTexture( GL_TEXTURE_2D, texture );
glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );


glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );
free( data );

return texture;
}


static void
drawBox(GLfloat size, GLenum type)
{
  static GLfloat n[6][3] =
  {
    {-1.0, 0.0, 0.0},
    {0.0, 1.0, 0.0},
    {1.0, 0.0, 0.0},
    {0.0, -1.0, 0.0},
    {0.0, 0.0, 1.0},
    {0.0, 0.0, -1.0}
  };
  static GLint faces[6][4] =
  {
    {0, 1, 2, 3},
    {3, 2, 6, 7},
    {7, 6, 5, 4},
    {4, 5, 1, 0},
    {5, 6, 2, 1},
    {7, 4, 0, 3}
  };
  GLfloat v[8][3];
  GLint i;

  v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
  v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
  v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
  v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
  v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
  v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;

  for (i = 5; i >= 0; i--) {
    glBegin(type);
    glNormal3fv(&n[i][0]);
    glTexCoord2f(0,0);
    glVertex3fv(&v[faces[i][0]][0]);
    glTexCoord2f(0,1);
    glVertex3fv(&v[faces[i][1]][0]);
    glTexCoord2f(1,1);
    glVertex3fv(&v[faces[i][2]][0]);
    glTexCoord2f(1,0);
    glVertex3fv(&v[faces[i][3]][0]);
    glEnd();
  }
}


static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //mat4 pMat = perspective(45.0f, 1.0f, 0.1f, 1500.0f);
    gluPerspective(45.0f, ar, 0.1f, 1500.0f);
    //glMultMatrixf(value_ptr(pMat));

    //mat4
    //glMultMatrixf(value_ptr(vMat));
}

int frame=0,t2,timebase=0;

static void display(void)
{
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const double a = t*90.0;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
    glPushMatrix();
        gluLookAt(2,4,2,0,0,0,0,1,0);
        glPushMatrix();
            glRotatef(45,0,0,1);
            glBegin(GL_TRIANGLES);
                for (int i=0;i<vertices.size(); i++)
                {
                    glm::vec3 v = vertices.at(i);
                    glNormal3fv(glm::value_ptr(normals.at(i)));
                    glTexCoord2fv(glm::value_ptr(uvs.at(i)));
                    glVertex3fv(glm::value_ptr(v));
                }
            glEnd();
        glPopMatrix();
    glPopMatrix();
    glutSwapBuffers();
    frame++;
	t2=glutGet(GLUT_ELAPSED_TIME);

	if (t2 - timebase > 5000) {
        float seconds = (t2-timebase) / 1000;
		float fps = frame/seconds;
		printf("FPS:%4.2f \n",fps);
	 	timebase = t2;
		frame = 0;
	}
}


static void idle(void)
{
    glutPostRedisplay();
}

const GLfloat light_ambient[]  = { 0.3f, 0.3f, 0.3f, 1.0f };
const GLfloat light_diffuse[]  = { 0.3f, 0.3f, 0.3f, 1.0f };
const GLfloat light_specular[] = { 0.3f, 0.3f, 0.3f, 1.0f };
const GLfloat light_position[] = { 5.0f, 0.0f, 0.0f, 1.0f };
const GLfloat mat_amb[] = {1.0f, 1.0f, 1.0f, 1.0f};
const GLfloat mat_dif[] = {1.0f, 1.0f, 1.0f, 1.0f};
const GLfloat mat_spec[] = {1.0f, 1.0f, 1.0f, 1.0f};

/* Program entry point */

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(400, 400);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutIdleFunc(idle);

    glClearColor(0, 0,0,0);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_amb );
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_dif );
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_spec );
    glLightf(GL_LIGHT0, GL_SHININESS, 0.3f);


    //tex = LoadTexture("Crate.bmp", 256,256);
    tex = SOIL_load_OGL_texture("dummy_wood.jpg", SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );    loadOBJ("cube.obj", vertices, uvs, normals);
    glutMainLoop();

    return EXIT_SUCCESS;
}

