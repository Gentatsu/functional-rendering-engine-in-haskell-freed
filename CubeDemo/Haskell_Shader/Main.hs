import Graphics.UI.GLUT hiding (Texture, rotate, perspective, lookAt)
import Data.IORef
import Bindings
import Graphics.FreeD
import System.Environment
import Data.Text hiding (map, concatMap, length, transpose)
import Graphics.GLUtil 
import Graphics.GLUtil.Camera3D
import Linear hiding (rotate)
import Foreign.Storable (sizeOf)
import Data.Map ((!))

varSize n = fromIntegral $ sizeOf (undefined::GLfloat) * n

main :: IO ()
main = do
	(_progName, _args) <- getArgsAndInitialize
	initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
	initialWindowSize $= Size 400 400
	_window <- createWindow ""
	reshapeCallback $= Just reshape
	depthFunc $= Just Less -- the comparison function for depth the buffer
	angle <- newIORef 0
	delta <- newIORef 0.05
	pos <- newIORef (45, 10)
	keyboardMouseCallback $= Just (keyboardMouse delta pos)
	objContents <- readFile "cube.obj"
	tex <- loadTex "dummy_wood.jpg" 
	t2 <- case tex of
		(Right x) -> return x
		(Left err) -> undefined
	let DObject c  mode =  loadObj $ pack objContents
	let eye' = V3 2 4 2
	let tow' = V3 0 0 0
	let up'  = V3 0 1 0
	let lightPos = Vertex4 5.0 0.0 0.0 1.0
	let amb' = Color4 0.3 0.3 0.3 1 :: Color4 GLfloat
	let dif' = Color4 0.3 0.3 0.3 1.0 :: Color4 GLfloat
	textureWrapMode Texture2D S $= (Repeated, Repeat)
	textureWrapMode Texture2D T $= (Repeated, Repeat)
	textureFilter   Texture2D      $= ((Nearest, Nothing), Nearest)
	textureBinding Texture2D $= Just t2
	let flat = flatten c
	let size = fromIntegral $ length flat
	let	stride = fromIntegral $ sizeOf (undefined::GLfloat) * 8
	buff <- makeBuffer ArrayBuffer flat
	p <- simpleShaderProgram "lt.vert" "lt.frag"
	;currentProgram $= Just (program p)
	;let posn = fst $ attribs p ! "vPosition"
	;let posVad = VertexArrayDescriptor 3 Float stride offset0
	;vertexAttribPointer posn $= (ToFloat, posVad)
	;vertexAttribArray posn   $= Enabled
	let r = 45 * (pi/180)
	let projMatrix = (projectionMatrix  r 1 0.1 1500 :: M44 GLfloat)
	let camMat = (lookAt eye' tow' up' :: M44 GLfloat)
	mvMatrix' <- getMV
	let mvMatrix =  camMat !*! rotate 45 0 0 1 mvMatrix'
	;setUniform p "uModelViewMatrix" $ mvMatrix 
	;setUniform p "uProjectionMatrix" $ projMatrix 
	;let normal = fst $ attribs p ! "vNormal"
	;let inv = inv44 $ mvMatrix 
	;let norVad = VertexArrayDescriptor 3 Float stride (offsetPtr (varSize 3))
	;vertexAttribPointer normal $= (ToFloat, norVad)
	;vertexAttribArray normal  $= Enabled
	case inv of 
		Just n -> 	do
			let nMatrix = transpose n
			setUniform p "uNormalMatrix" $ nMatrix
	;setUniform p "uLightPos" $ (lightPos :: (Vertex4 GLfloat))
	;setUniform p "uLightAmb" $ amb'
	;setUniform p "uLightDif" $ dif'  
	;let texCoord = fst $ attribs p ! "vTexCoord"
	;let texVad = VertexArrayDescriptor 2 Float stride (offsetPtr (varSize 6))
	;vertexAttribPointer texCoord $= (ToFloat, texVad)
	;vertexAttribArray texCoord  $= Enabled
	;activeTexture $= TextureUnit 0
	;setUniform p "uTexUnit" (TextureUnit 0)

	state <- makeState
	idleCallback $= Just (idle angle delta)
	displayCallback $= display {-angle pos contents-}size  state
	mainLoop

flatten :: DGeometry -> [GLfloat]
flatten (DGeometryV vs) = concatMap (\(Vertex3 v1 v2 v3) -> [v1, v2, v3]) vs
flatten (DGeometryVn vns) = concatMap (\(Vertex3 v1 v2 v3, Normal3 n1 n2 n3) -> [v1, v2, v3, n1, n2, n3]) vns
flatten (DGeometryVt vns) = concatMap (\(Vertex3 v1 v2 v3, TexCoord2 t1 t2) -> [v1, v2, v3, t1, t2]) vns
flatten (DGeometryVnt vns)= concatMap (\(Vertex3 v1 v2 v3, Normal3 n1 n2 n3, TexCoord2 t1 t2) -> [v1, v2, v3, n1, n2, n3, t1, t2]) vns
flatten (DGeometryNv (Normal3 n1 n2 n3) vs)= concatMap (\(Vertex3 v1 v2 v3) -> [v1, v2, v3, n1, n2, n3]) vs
flatten (DGeometryNt nts) = concatMap (\(Normal3 n1 n2 n3, (Vertex3 v1 v2 v3, Vertex3 v4 v5 v6, Vertex3 v7 v8 v9)) -> [v1, v2, v3, n1, n2, n3, v4,v5,v6,n1,n2,n3,v7,v8,v9, n1, n2, n3]) nts

										
-- Gets current ModelView matrix on stack.
getMV :: IO (M44 GLfloat)
getMV = 	do
										mvMatrix <- get ((matrix $ Just $ Modelview 0)::StateVar(GLmatrix GLfloat))
										mv <- getMatrixComponents ColumnMajor mvMatrix
										let mvm = getMat mv
										return mvm

-- Gets current Project matrix on stack.
getP :: IO (M44 GLfloat)
getP = 	do
										pMatrix <- get ((matrix $ Just Projection)::StateVar(GLmatrix GLfloat))
										pm <-	getMatrixComponents ColumnMajor pMatrix
										let pM = getMat pm 
										return pM

-- Converts list of floats in to a 4x4 matrix.										
getMat :: [GLfloat] -> M44 GLfloat
getMat (a11:a12:a13:a14: 
				a21:a22:a23:a24:
				a31:a32:a33:a34:
				a41:a42:a43:a44:_) = V4 (V4 a11 a12 a13 a14)
                 (V4 a21 a22 a23 a24)
                 (V4 a31 a32 a33 a34)
                 (V4 a41 a42 a43 a44)


-- Convert a Unit Quaternion to a 4x4 Column-Major OpenGL Matrix
getMatrix :: Num a => Quaternion a -> M44 a
getMatrix (Quaternion w (V3 x y z))
    = V4 (V4 (1 - 2*(yy+zz))  (2*(xy-zw))     (2*(yw+xz))       0)
           (V4 (2*(xy+zw))      (1 - 2*(xx+zz)) (2*(yz-xw))       0)
           (V4 (2*(xz-yw))      (2*(yz+xw))     (1 - 2*(xx + yy)) 0)
           (V4 0                0               0                 1)
      where           
          xx      = x * x
          xy      = x * y
          xz      = x * z
          xw      = x * w

          yy      = y * y
          yz      = y * z
          yw      = y * w

          zz      = z * z
          zw      = z * w
          ww      = w * w

          -- Performs a rotation on matrix in state, given angle, x,y,z axis varibles.
rotate ::GLfloat -> GLfloat -> GLfloat -> GLfloat -> M44 GLfloat ->  M44 GLfloat
rotate a x y z mv =   mv !*! getMatrix (axisAngle (V3 x y z) r) 
	where r = (pi / 180.0) * a

												
