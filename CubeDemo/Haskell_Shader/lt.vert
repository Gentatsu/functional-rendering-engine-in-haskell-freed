#version 330

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;


out vec4 vColor;
out vec2 fTexCoord;

uniform vec4 uLightPos;
uniform vec4 uLightAmb;
uniform vec4 uLightDif;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat4 uNormalMatrix;


void main()
{   
    //vec4 eyeNorm = normalize(uNormalMatrix * vec4(vNormal, 0.0));
    mat3 mv3 = mat3(uModelViewMatrix);
    mat3 nMatrix = transpose(inverse(mv3));
		vec4 eyeNorm = vec4(normalize(nMatrix * vNormal), 0.0);
    vec4 eyeCord= uModelViewMatrix * vec4(vPosition, 1.0);

    fTexCoord = vTexCoord;

    vec4 uLightSpec = vec4(0.3,0.3,0.3,1);
    vec4 uMatAmb = vec4(1,1,1,1);
    vec4 uMatSpec = vec4(1,1,1,1);
    vec4 uMatDif = vec4(1,1,1,1);
    vec4 s = normalize(uLightPos - eyeCord) ;
    vec4 r = normalize(reflect(-s,eyeNorm));
    vec4 v = normalize(-eyeCord);

    float diff = max(dot(eyeNorm,s),0.0);

    vec4 h = (s + v) / 2;
    float spec = max( dot(eyeNorm, h),0.0 );

    vec4 diffColour = clamp(diff * uLightDif * uMatDif, 0.0, 1.0);
    vec4 specColour = clamp(pow(spec,0.3) * uLightSpec * uMatSpec, 0.0, 1.0);
    vec4 ambientColour = clamp(uLightAmb * uMatAmb, 0.0, 1.0);

    vColor = specColour + ambientColour + diffColour;

    gl_Position = uProjectionMatrix * uModelViewMatrix * vec4( vPosition,1.0);
}
