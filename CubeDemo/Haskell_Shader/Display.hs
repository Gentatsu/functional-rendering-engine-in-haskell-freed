module Display (idle, display, State(..), makeState) where
 
import Graphics.UI.GLUT hiding (Color, Cube, Texture)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Graphics.GLUtil


data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

display :: NumArrayIndices ->  State -> DisplayCallback
display size state = do 
	clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
	loadIdentity
	drawArrays Triangles 0 $ size `div` 8
	swapBuffers
	frames state $~! (+1)
	t0' <- get (t0 state)
	t <- get elapsedTime
	when (t - t0' >= 5000) $ do
		f <- get (frames state)
		let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		let fps = fromIntegral f / seconds
		putStrLn (show fps)
		t0 state $= t
		frames state $= 0

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  
getCamPos :: GLfloat -> (GLfloat, GLfloat)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360



					
