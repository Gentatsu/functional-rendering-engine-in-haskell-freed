#version 330

in vec2 fTexCoord;
in vec4 vColor;

out vec4 fColour;



uniform sampler2D uTexUnit;

void main()
{
    vec4 texColour = texture(uTexUnit, fTexCoord);
    fColour =  vColor * texColour; 
}
