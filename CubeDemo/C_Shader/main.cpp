/*
 * GLUT Shapes Demo
 *
 * Written by Nigel Stewart November 2003
 *
 * This program is test harness for the sphere, cone
 * and torus shapes in GLUT.
 *
 * Spinning wireframe and smooth shaded shapes are
 * displayed until the ESC or q key is pressed.  The
 * number of geometry stacks and slices can be adjusted
 * using the + and - keys.
 */

#define GLEW_STATIC
#include "glew.h"


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include "SOIL.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
using namespace glm;
#include "objloader.hpp"
#include <vector>

std::vector<glm::vec3> vertices;
std::vector<glm::vec2> uvs;
std::vector<glm::vec3> normals;
    GLuint m_VAO;

GLuint tex;
/* GLUT callback Handlers */

GLuint prog;

GLuint loadShader (std::string vertexSource, std::string fragmentSource)
{
//Create an empty vertex shader handle
GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

//Send the vertex shader source code to GL
//Note that std::string's .c_str is NULL character terminated.
const GLchar *source = (const GLchar *)vertexSource.c_str();
glShaderSource(vertexShader, 1, &source, 0);

//Compile the vertex shader
glCompileShader(vertexShader);

GLint isCompiled = 0;
glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
if(isCompiled == GL_FALSE)
{
	GLint maxLength = strlen(source);
	glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

	//The maxLength includes the NULL character
	std::vector<GLchar> infoLog(maxLength);
	glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);
	for (int i=0;i<infoLog.size(); i++)
    {
        printf("%c", infoLog[i]);
    }
    printf("\n");

	//We don't need the shader anymore.
	glDeleteShader(vertexShader);

	//Use the infoLog as you see fit.

	//In this simple program, we'll just leave
	return 0;
}

//Create an empty fragment shader handle
GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

//Send the fragment shader source code to GL
//Note that std::string's .c_str is NULL character terminated.
source = (const GLchar *)fragmentSource.c_str();
glShaderSource(fragmentShader, 1, &source, 0);

//Compile the fragment shader
glCompileShader(fragmentShader);

glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);
if(isCompiled == GL_FALSE)
{
	GLint maxLength = strlen(source);
	glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);

	//The maxLength includes the NULL character
	std::vector<GLchar> infoLog(maxLength);
	glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &infoLog[0]);
    for (int i=0;i<infoLog.size(); i++)
    {
        printf("%c", infoLog[i]);
    }
    printf("\n");
	//We don't need the shader anymore.
	glDeleteShader(fragmentShader);
	//Either of them. Don't leak shaders.
	glDeleteShader(vertexShader);

	//Use the infoLog as you see fit.

	//In this simple program, we'll just leave
	return 0;
}

//Vertex and fragment shaders are successfully compiled.
//Now time to link them together into a program.
//Get a program object.
GLuint program = glCreateProgram();

//Attach our shaders to our program
glAttachShader(program, vertexShader);
glAttachShader(program, fragmentShader);

//Link our program
glLinkProgram(program);

//Note the different functions here: glGetProgram* instead of glGetShader*.
GLint isLinked = 0;
glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
if(isLinked == GL_FALSE)
{
	GLint maxLength = strlen(source);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

	//The maxLength includes the NULL character
	std::vector<GLchar> infoLog(maxLength);
	glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
    for (int i=0;i<infoLog.size(); i++)
    {
        printf("%c", infoLog[i]);
    }
    printf("\n");
	//We don't need the program anymore.
	glDeleteProgram(program);
	//Don't leak shaders either.
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	//Use the infoLog as you see fit.

	//In this simple program, we'll just leave
	return 0;
}

//Always detach shaders after a successful link.
glDetachShader(program, vertexShader);
glDetachShader(program, fragmentShader);

return program;
}
GLuint LoadTexture( const char * filename, int width, int height )
{

  GLuint texture;


  unsigned char * data;

  FILE * file;

  file = fopen( filename, "rb" );

  if ( file == NULL ) return 0;
  data = (unsigned char *)malloc( width * height * 3 );
  //int size = fseek(file,);
  fread( data, width * height * 3, 1, file );
  fclose( file );

 for(int i = 0; i < width * height ; ++i)
{
   int index = i*3;
   unsigned char B,R;
   B = data[index];
   R = data[index+2];

   data[index] = R;
   data[index+2] = B;

}



glGenTextures( 1, &texture );
glBindTexture( GL_TEXTURE_2D, texture );
glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );


glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );
free( data );

return texture;
}

char* readFile(const char* filename)
{
  FILE* input = fopen(filename, "rb");
  if(input == NULL) return NULL;

  if(fseek(input, 0, SEEK_END) == -1) return NULL;
  long size = ftell(input);
  if(size == -1) return NULL;
  if(fseek(input, 0, SEEK_SET) == -1) return NULL;

  /*if using c-compiler: dont cast malloc's return value*/
  char *content = (char*) malloc( (size_t) size +1  );
  if(content == NULL) return NULL;

  fread(content, 1, (size_t)size, input);
  if(ferror(input)) {
    free(content);
    return NULL;
  }

  fclose(input);
  content[size] = '\0';
  return content;
}

static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //mat4 pMat = perspective(45.0f, 1.0f, 0.1f, 1500.0f);
    gluPerspective(45.0f, ar, 0.1f, 1500.0f);
    //glMultMatrixf(value_ptr(pMat));

    //mat4
    //glMultMatrixf(value_ptr(vMat));
}

int frame=0,t2,timebase=0;

static void display(void)
{
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const double a = t*90.0;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
    glPushMatrix();
        gluLookAt(2,4,2,0,0,0,0,1,0);
        glPushMatrix();
            glRotatef(45,0,0,1);
            //mat4 mvMat = lookAt(vec3(2,4,2), vec3(0,0,0), vec3(0,1,0));
            //mvMat = rotate(mvMat, 45.0f, vec3(0,0,1));
            //glMultMatrixf(value_ptr(mvMat));
            //glEnable(GL_TEXTURE_GEN_S); //enable texture coordinate generation
            //glEnable(GL_TEXTURE_GEN_T);
            glBindTexture(GL_TEXTURE_2D, tex);
//            glBegin(GL_TRIANGLES);
//                for (int i=0;i<vertices.size(); i++)
//                {
//                    glm::vec3 v = vertices.at(i);
//                    glNormal3fv(glm::value_ptr(normals.at(i)));
//                    glTexCoord2fv(glm::value_ptr(uvs.at(i)));
//                    glVertex3fv(glm::value_ptr(v));
//                }
//            glEnd();
            glDrawArrays(GL_TRIANGLES, 0, vertices.size());
            //drawBox(1, GL_QUADS);
            //glutSolidCube(1);
        glPopMatrix();
    glPopMatrix();
    glutSwapBuffers();
    frame++;
	t2=glutGet(GLUT_ELAPSED_TIME);

	if (t2 - timebase > 5000) {
        float seconds = (t2-timebase) / 1000;
		float fps = frame/seconds;
		printf("FPS:%4.2f \n",fps);
	 	timebase = t2;
		frame = 0;
	}
}


static void idle(void)
{
    glutPostRedisplay();
}

const GLfloat light_ambient[]  = { 0.3f, 0.3f, 0.3f, 1.0f };
const GLfloat light_diffuse[]  = { 0.3f, 0.3f, 0.3f, 1.0f };

const GLfloat light_specular[] = { 0.3f, 0.3f, 0.3f, 1.0f };
const GLfloat light_position[] = { 5.0f, 0.0f, 0.0f, 1.0f };

const GLfloat mat_amb[] = {1.0f, 1.0f, 1.0f, 1.0f};
const GLfloat mat_dif[] = {1.0f, 1.0f, 1.0f, 1.0f};
const GLfloat mat_spec[] = {1.0f, 1.0f, 1.0f, 1.0f};

/* Program entry point */

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(400, 400);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutIdleFunc(idle);

    glClearColor(0, 0,0,0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glewExperimental = GL_TRUE;
    glewInit();
    tex = SOIL_load_OGL_texture("dummy_wood.jpg", SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
    loadOBJ("cube.obj", vertices, uvs, normals);
    prog = loadShader(readFile("lt.vert"), readFile("lt.frag"));
    glUseProgram(prog);
    glGenVertexArrays(1, &m_VAO);
    glBindVertexArray(m_VAO);
    GLuint posBuf;
    GLuint norBuf;
    GLuint texBuf;
    glGenBuffers(1, &posBuf);
    glBindBuffer(GL_ARRAY_BUFFER, posBuf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
    GLint posAttrib = glGetAttribLocation(prog, "vPosition");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glGenBuffers(1, &texBuf);
    glBindBuffer(GL_ARRAY_BUFFER, texBuf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * uvs.size(), &uvs[0], GL_STATIC_DRAW);
    GLint texAttrib = glGetAttribLocation(prog, "vTexCoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glGenBuffers(1, &norBuf);
    glBindBuffer(GL_ARRAY_BUFFER, norBuf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * normals.size(), &normals[0], GL_STATIC_DRAW);
    GLint norAttrib = glGetAttribLocation(prog, "vNormal");
    glEnableVertexAttribArray(norAttrib);
    glVertexAttribPointer(norAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    GLint loc;
    loc = glGetUniformLocation(prog,"uTexUnit");
	glUniform1i(tex, 0);
	loc = glGetUniformLocation(prog,"uLightPos");
	glUniform4fv(loc,1,light_position);
	loc = glGetUniformLocation(prog,"uLightAmb");
	glUniform4fv(loc, 1, light_ambient);
	loc = glGetUniformLocation(prog,"uLightDif");
	glUniform4fv(loc,1,light_diffuse);
    mat4 mvMat = lookAt(vec3(2,4,2), vec3(0,0,0), vec3(0,1,0));
    mvMat = rotate(mvMat, 45.0f, vec3(0,0,1));
    mat4 pMat = perspective(45.0f, 1.0f, 0.1f, 1500.0f);
    mat4 nMat = inverseTranspose(mvMat);
	loc = glGetUniformLocation(prog,"uModelViewMatrix");
	glUniformMatrix4fv(loc,1,FALSE, &mvMat[0][0]);
	loc = glGetUniformLocation(prog,"uProjectionMatrix");
	glUniformMatrix4fv(loc,1,FALSE, &pMat[0][0]);
    loc = glGetUniformLocation(prog,"uNormalMatrix");
	glUniformMatrix4fv(loc,1,FALSE, &nMat[0][0]);

    glutMainLoop();

    return EXIT_SUCCESS;
}

