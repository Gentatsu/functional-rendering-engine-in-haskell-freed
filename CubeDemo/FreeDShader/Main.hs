import Graphics.UI.GLUT hiding (Texture)
import Data.IORef
import Bindings
import Graphics.FreeD
import System.Environment
import Data.Text hiding (map)
import Graphics.GLUtil 
import Linear 

main :: IO ()
main = do
	(_progName, _args) <- getArgsAndInitialize
	initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
	initialWindowSize $= Size 400 400
	_window <- createWindow ""
	reshapeCallback $= Just reshape
	depthFunc $= Just Less -- the comparison function for depth the buffer
	angle <- newIORef 0
	delta <- newIORef 0.05
	pos <- newIORef (45, 10)
	keyboardMouseCallback $= Just (keyboardMouse delta pos)
	objContents <- readFile "cube.obj"
	tex <- loadTex "dummy_wood.jpg" 
	t2 <- case tex of
		(Right x) -> return x
		(Left err) -> undefined
	let c =  loadObj $ pack objContents
	let eye' = V3 2 4 2
	let tow' = V3 0 0 0
	let up'  = V3 0 1 0
	let lightPos = Vertex4 5.0 0.0 0.0 1.0
	let amb' = Color4 0.3 0.3 0.3 1 
	let dif' = Color4 0.3 0.3 0.3 1.0
	let spec' = Color4 0.3 0.3 0.3 1.0
	let view = DView {eye=eye', tow=tow', up=up', fov=45, aspectRatio=1, znear=0.1, zfar=1500}
	let lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Gourad}
	d <- compile $ Texture t2 $ Camera view $ Lighting lighting $ Rotate 45 0 0 1 c
	state <- makeState
	idleCallback $= Just (idle angle delta)
	displayCallback $= display {-angle pos contents-}d state
	mainLoop


