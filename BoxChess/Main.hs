import Graphics.UI.GLUT
import Data.IORef
import Bindings
import Graphics.GLUtil

main :: IO ()
main = do
	(_progName, _args) <- getArgsAndInitialize
	initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
	initialWindowSize $= Size 800 800
	_window <- createWindow "Box"
	reshapeCallback $= Just reshape
	pos <- newIORef (0.0, 0.0, 0.0)
	t <- loadTex "Wood.jpg"
	tex <- newIORef $ t
	keyboardMouseCallback $= Just (keyboardMouse angles pos)
	idleCallback $= Just (idle pos)
	state <- makeState
	displayCallback $= display pos tex state
	mainLoop

loadTex :: FilePath -> IO TextureObject
loadTex filename = do
										t <- readTexture filename
										textureWrapMode Texture2D S $= (Repeated, Repeat)
										textureWrapMode Texture2D T $= (Repeated, Repeat)
										textureFilter   Texture2D      $= ((Nearest, Nothing), Nearest) 
										return $ (\(Right x) -> x) t
