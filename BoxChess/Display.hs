module Display (idle, display, initi, State(..), makeState) where
 
import Graphics.UI.GLUT hiding (Color, Cube, lookAt, Texture, ShadingModel, Flat, Sphere)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Data.Text hiding (transpose, map, length, replicate, concat)
import Graphics.GLUtil
import Foreign.Storable (sizeOf)
import Data.Map ((!), keys)
import Linear 

data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

display :: IORef (GLfloat, GLfloat, GLfloat) -> IORef TextureObject ->State -> DisplayCallback
display posi tex state= do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  ;(x,y,z) <- get posi
  ;wood <- get tex
  let
    eye' = V3 x y (z+2)
    dir = V3 0 5 (-2)
    up' = V3 0 1 0 
    view = DView {eye=eye', tow=(dir+eye'), up=up', fov=90, aspectRatio=1.0, znear=0.1, zfar=1500}
    floor = Cube 20 20 1
    left = Translate 21 0 19 $ Rotate 90 0 1 0 floor
    right = Translate (-21) 0 19 $ Rotate 90 0 1 0 floor
    ceil = Translate 0 0 39 floor
    back = Translate 0 21 19 $ Rotate 90 1 0 0 floor
  ;render $ Camera view $ Texture wood $ Drawables [floor, left, right, ceil, back]
  ;swapBuffers
  ;frames state $~! (+1)
	;t0' <- get (t0 state)
	;t <- get elapsedTime
	;when (t - t0' >= 5000) $ do
		;f <- get (frames state)
		;let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		;let fps = fromIntegral f / seconds
		;putStrLn (show fps)
		;t0 state $= t
		;frames state $= 0

idle :: IORef (GLfloat, GLfloat, GLfloat) -> IdleCallback
idle p = do
  (x,y,z) <- get p
  unless (z <= 0.0) $ do 
  	p $~! \(x,y,z) -> (x,y,z-0.01)
  postRedisplay Nothing
