module Bindings (idle, display, reshape, keyboardMouse, initi, State(..), makeState, mouse) where
 
import Graphics.UI.GLUT
import Data.IORef
import Display
import Control.Monad

reshape :: ReshapeCallback
reshape size = do 
  viewport $= (Position 0 0, size)
 
keyboardMouse :: IORef (GLfloat, GLfloat) -> IORef (GLfloat, GLfloat, GLfloat) -> KeyboardMouseCallback
keyboardMouse a p key Down _ (Position px py) = do
	(x,y,z) <- get p
	case key of
		(Char ' ') -> p $~! \(x,y,z) -> (x,y,z+5)
		(SpecialKey KeyLeft ) -> unless (x <= (-20.0)) $ p $~! \(x,y,z) -> (x-1,y,z)
		(SpecialKey KeyRight) -> unless (x >= 20.0) $ p $~! \(x,y,z) -> (x+1,y,z)
		(SpecialKey KeyUp   ) -> unless (y >= 20.0) $ p $~! \(x,y,z) -> (x,y+1,z)
		(SpecialKey KeyDown ) -> unless (y <= (-20.0)) $ p $~! \(x,y,z) -> (x,y-1,z)
		_ -> return ()
keyboardMouse _ _ key Up _ _ = 
	case key of 
		(MouseButton LeftButton) ->  pointerPosition $= (Position 400 400)
		_ -> return ()
keyboardMouse _ _ _ _ _ _ = return ()


mouse :: IORef (GLfloat, GLfloat) -> MotionCallback
mouse a (Position px py) = do
	(ha, va) <- get a
	let hoz = 0.0005 * (400 - (fromIntegral px))
	let ver = 0.0005 * (400 - (fromIntegral py))
	a $~! \(ha, va) -> (ha+ hoz, va+ ver)


	
