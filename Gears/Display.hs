module Display (idle, display) where
 
import Graphics.UI.GLUT hiding (Color, Cube, Sphere, Flat)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD

display :: IORef GLfloat -> IORef (GLdouble, GLdouble) -> DisplayCallback
display angle pos = do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  loadIdentity
  (x',y') <- get pos
  --translate $ Vector3 x' y' 0
  preservingMatrix $ do
		a <- get angle
		let p = points 1
		let eye' = Vertex3 0 0 (-40)
		let tow' = Vertex3 0 0 0
		let up'  = Vector3 0 1 0
		let lightPos = Vertex4 5 5 10 0
		let amb' = Color4 0.0 0.0 0.0 1.0
		let dif' = Color4 1.0 1.0 1.0 1.0
		let spec' = Color4 0.0 0.0 0.0 1.0
		let view = DView {eye=eye', tow=tow', up=up', fov=x', aspect_ratio=1, znear=5, zfar=60}
		let lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Flat}
    --renderDrawable $ Camera view $ Lighting lighting  
     -- make the gears
		g1 <- defineNewList Compile $ do
			materialAmbientAndDiffuse Front $= Color4 0.8 0.1 0.0 1.0
			gear 1 4 1 20 0.7
		g2 <- defineNewList Compile $ do
			materialAmbientAndDiffuse Front $= Color4 0.0 0.8 0.2 1.0
			gear 0.5 2 2 10 0.7
		g3 <- defineNewList Compile $ do
			materialAmbientAndDiffuse Front $= Color4 0.2 0.2 1.0 1.0
			gear 1.3 2 0.5 10 0.7
		let gears = Drawables [Translate (-3) (-2) 0 $ Rotate a 0 0 1 $ Compiled g1, Translate 3.1 (-2) 0 $ Rotate (-2 * a - 9) 0 0 1 $  Compiled g2, Translate (-3.1) 4.2 0 $ Rotate (-2 * a - 25) 0 0 1 $ Compiled g3]
		renderDrawable $ Camera view $ Lighting lighting gears
  swapBuffers
  flush
 
idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  

gear :: GLfloat -> GLfloat -> GLfloat -> GLint -> GLfloat -> IO()
gear innerRadius outerRadius width teeth toothDepth = do
   let r0 = innerRadius
       r1 = outerRadius - toothDepth / 2
       r2 = outerRadius + toothDepth / 2

       da = 2 * pi / fromIntegral teeth / 4
       w  = 0.5 * width

       render p f =
          renderPrimitive p $
             mapM_ (\i -> let angle = fromIntegral i * 2 * pi / fromIntegral teeth
                              in f r0 r1 r2 w da teeth i angle)
                   [ 0 .. teeth ]

   shadeModel $= Flat
   currentNormal $= Normal3 0 0 1
   render QuadStrip gearFront
   render Quads     teethFront
   currentNormal $= Normal3 0 0 (-1)
   render QuadStrip gearBack
   render Quads     teethBack
   render QuadStrip teethFace
   shadeModel $= Smooth
   render QuadStrip gearInside

type Renderer =
   GLfloat -> GLfloat -> GLfloat -> GLfloat -> GLfloat -> GLint -> GLint -> GLfloat -> IO ()

-- draw front face
gearFront :: Renderer
gearFront r0 r1 _ w da teeth i angle = do
   vertex (Vertex3 (r0 * cos angle) (r0 * sin angle) w)
   vertex (Vertex3 (r1 * cos angle) (r1 * sin angle) w)
   when (i < teeth) $ do
      vertex (Vertex3 (r0 * cos  angle          ) (r0 * sin  angle          ) w)
      vertex (Vertex3 (r1 * cos (angle + 3 * da)) (r1 * sin (angle + 3 * da)) w)

-- draw front sides of teeth
teethFront :: Renderer
teethFront _ r1 r2 w da teeth i angle =
   when (i < teeth) $ do
      vertex (Vertex3 (r1 * cos  angle          ) (r1 * sin  angle          ) w)
      vertex (Vertex3 (r2 * cos (angle +     da)) (r2 * sin (angle +     da)) w)
      vertex (Vertex3 (r2 * cos (angle + 2 * da)) (r2 * sin (angle + 2 * da)) w)
      vertex (Vertex3 (r1 * cos (angle + 3 * da)) (r1 * sin (angle + 3 * da)) w)

-- draw back face
gearBack :: Renderer
gearBack r0 r1 _ w da teeth i angle = do
   vertex (Vertex3 (r1 * cos angle) (r1 * sin angle) (-w))
   vertex (Vertex3 (r0 * cos angle) (r0 * sin angle) (-w))
   when (i < teeth) $ do
      vertex (Vertex3 (r1 * cos (angle + 3 * da)) (r1 * sin (angle + 3 * da)) (-w))
      vertex (Vertex3 (r0 * cos  angle          ) (r0 * sin  angle          ) (-w))

-- draw back sides of teeth
teethBack :: Renderer
teethBack _ r1 r2 w da teeth i angle =
   when (i < teeth) $ do
      vertex (Vertex3 (r1 * cos (angle + 3 * da)) (r1 * sin (angle + 3 * da)) (-w))
      vertex (Vertex3 (r2 * cos (angle + 2 * da)) (r2 * sin (angle + 2 * da)) (-w))
      vertex (Vertex3 (r2 * cos (angle +     da)) (r2 * sin (angle +     da)) (-w))
      vertex (Vertex3 (r1 * cos  angle          ) (r1 * sin  angle          ) (-w))

-- draw outward faces of teeth
teethFace :: Renderer
teethFace _ r1 r2 w da teeth i angle =
  if (i < teeth) then do
    vertex (Vertex3 (r1*(cos angle)) (r1*(sin angle)) w)
    vertex (Vertex3 (r1*(cos angle)) (r1*(sin angle)) (-w))

    let u'    = r2 * cos (angle + da) - r1 * cos angle
        v'    = r2 * sin (angle + da) - r1 * sin angle
        len   = sqrt (u' * u' + v' * v')
        u     = u' / len
        v     = v' / len
    currentNormal $= Normal3 v (-u) 0
    vertex (Vertex3 (r2 * cos (angle +     da)) (r2 * sin (angle +     da))   w )
    vertex (Vertex3 (r2 * cos (angle +     da)) (r2 * sin (angle +     da)) (-w))
    currentNormal $= Normal3 (cos angle) (sin angle) 0
    vertex (Vertex3 (r2 * cos (angle + 2 * da)) (r2 * sin (angle + 2 * da))   w )
    vertex (Vertex3 (r2 * cos (angle + 2 * da)) (r2 * sin (angle + 2 * da)) (-w))
    let u2    = r1 * cos (angle + 3 * da) - r2 * cos (angle + 2 * da);
        v2    = r1 * sin (angle + 3 * da) - r2 * sin (angle + 2 * da);
    currentNormal $= Normal3 v2 (-u2) 0
    vertex (Vertex3 (r1 * cos (angle + 3 * da)) (r1 * sin (angle + 3 * da))   w )
    vertex (Vertex3 (r1 * cos (angle + 3 * da)) (r1 * sin (angle + 3 * da)) (-w))
    currentNormal $= Normal3 (cos angle) (sin angle) 0
  else do
    vertex (Vertex3 (r1 * cos 0) (r1 * sin 0)   w )
    vertex (Vertex3 (r1 * cos 0) (r1 * sin 0) (-w))

-- draw inside radius cylinder
gearInside :: Renderer
gearInside r0 _ _ w _ _ _ angle = do
   currentNormal $= Normal3 (-cos angle) (-sin angle) 0
   vertex (Vertex3 (r0 * cos angle) (r0 * sin angle) (-w))
   vertex (Vertex3 (r0 * cos angle) (r0 * sin angle)   w )


