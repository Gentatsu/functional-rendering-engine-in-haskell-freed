
{-# LANGUAGE FlexibleInstances, BangPatterns, FlexibleContexts #-}

module TetMeshRenderer 
  ( renderVertices
  , renderEdges
  , renderFaces
  , renderIsosurface
  , renderFiber
  , renderReebSpace
  ) where

-- | Haskell data and control modules.
import           Control.Applicative
import           Control.Monad
import           Data.Array
import           Data.Maybe
import           Data.Vect.Floating

-- | Graphics and rendering.
import qualified Graphics.Rendering.OpenGL as GL
import           Graphics.Rendering.OpenGL.GL.BufferObjects
import           Graphics.GLUtil

-- | PROJECT-SPECIFIC modules.
import           Mesh
import           FiberSurface

-- | Utility functions for simplifying GL color, vertex normal specification.
--

color4f :: GL.GLfloat -> GL.GLfloat -> GL.GLfloat -> GL.GLfloat -> IO ()
color4f r g b a = GL.color $ GL.Color4 r g b a

vertex3f :: GL.GLfloat -> GL.GLfloat -> GL.GLfloat -> IO ()
vertex3f x y z = GL.vertex $ GL.Vertex3 x y z

vertex3fv :: Coord -> IO ()
vertex3fv (Vec4 x y z _) = GL.vertex $ GL.Vertex3 (realToFrac x) (realToFrac y) (realToFrac z :: GL.GLfloat)

normal3fv :: Vec3 GL.GLfloat -> IO ()
normal3fv (Vec3 x y z) = GL.normal $ GL.Normal3 (realToFrac x) (realToFrac y) (realToFrac z :: GL.GLfloat)


-- | Low-level rendering actions: triangles and lines.
--

renderTriangle :: SimplexT Coord -> IO ()
renderTriangle (S2 u v w)
    = do { let norm = ((trim u :: Vec3 GL.GLfloat) &- trim v) &^ (trim u &- trim w)
         ; normal3fv norm
         ; vertex3fv u 
         ; vertex3fv v
         ; vertex3fv w
         }

renderLine :: SimplexT Coord -> IO ()
renderLine (S1 u v)
    = do { vertex3fv u 
         ; vertex3fv v
         }


-- | Render mesh structures: vertices, edges and faces.
--

renderVertices :: TetMesh -> IO ()
renderVertices m
    = do { GL.lighting GL.$= GL.Disabled
         ; GL.pointSize GL.$= 7.0
         ; color4f 1.0 0.0 0.0 1.0
         ; GL.renderPrimitive GL.Points $
             forM_ (map dom . elems . points $ m) $ vertex3fv
         ; GL.pointSize GL.$= 1.0
         ; GL.lighting GL.$= GL.Enabled
         }

renderEdges :: TetMesh -> IO ()
renderEdges m
    = do { GL.lighting GL.$= GL.Disabled
         ; GL.lineWidth GL.$= 2.0
         ; color4f 0.0 1.0 0.0 1.0
         ; GL.renderPrimitive GL.Lines $
             forM_ (s1 m) $ \(S1 u v) ->
               do { vertex3fv . dom . point m $ u
                  ; vertex3fv . dom . point m $ v
                  }
         ; GL.lineWidth GL.$= 1.0
         ; GL.lighting GL.$= GL.Enabled
         }

renderFaces :: TetMesh -> IO ()
renderFaces m
    = do { GL.lighting GL.$= GL.Enabled
         ; color4f 0.0 0.4 0.4 0.5
         ; GL.renderPrimitive GL.Triangles $ 
             forM_ (s2 m) $ \s ->
               renderTriangle . fmap (dom . point m) $ s
         ; GL.lighting GL.$= GL.Disabled
         }

-- | Rendering of surfaces and fibers.
-- 

renderIsosurface :: [SimplexT Coord] -> GL.Color4 GL.GLfloat -> IO ()
renderIsosurface ss col
    = do { GL.lighting GL.$= GL.Enabled
         ; GL.materialAmbientAndDiffuse GL.FrontAndBack GL.$= col
         ; GL.renderPrimitive GL.Triangles $ forM_ ss renderTriangle
         ; GL.lighting GL.$= GL.Disabled
         }

renderFiber :: [SimplexT Coord] -> GL.Color4 GL.GLfloat -> IO ()
renderFiber ss col
    = do { GL.lighting GL.$= GL.Disabled
         ; GL.color $ col
         ; GL.lineWidth GL.$= 5.0 
         ; GL.renderPrimitive GL.Lines $
             forM_ ss $ renderLine
         ; GL.lineWidth GL.$= 1.0
         ; GL.lighting GL.$= GL.Enabled
         }                   

renderReebSpace :: TetMesh -> BufferObject -> BufferObject -> IO ()
renderReebSpace m vb eb -- vb
    = do { GL.lighting GL.$= GL.Disabled
         ; GL.depthFunc GL.$= Nothing
         ; GL.blend GL.$= GL.Enabled
         ; GL.blendFunc GL.$= (GL.SrcAlpha, GL.OneMinusSrcAlpha)
         ; GL.blendEquation GL.$= GL.FuncAdd
         ; GL.pointSize GL.$= 7.0
         ; GL.lineWidth GL.$= 2.0
         ; GL.clientState GL.VertexArray GL.$= GL.Enabled

         ; GL.bindBuffer GL.ArrayBuffer GL.$= Just vb
         ; GL.bindBuffer GL.ElementArrayBuffer GL.$= Just eb

         ; color4f 1.0 0.0 0.0 0.1
         ; GL.pointSize GL.$= 10.0
         ; GL.renderPrimitive GL.Points $ 
             forM_ (s0 m) $ \ (S0 i) ->
                let (Vec4 xi yi _ _) = rng ((points m) ! i)
                in when (not (isNaN xi) && not (isNaN yi)) $
                       GL.vertex $ GL.Vertex2 xi yi

         ; color4f 0.0 0.0 0.0 0.2
         -- Currently not supported: show outline of triangles.
         -- When mesh becomes non-trivial, edge density becomes too dense.
         -- ; GL.drawElements GL.Lines     (fromIntegral (length (s1 m) * 2)) GL.UnsignedInt offset0   
         ; GL.drawElements GL.Triangles (fromIntegral (length (s2 m) * 3)) GL.UnsignedInt offset0   

         ; GL.pointSize GL.$= 1.0
         ; GL.lineWidth GL.$= 1.0
         ; GL.blend GL.$= GL.Disabled
         ; GL.clientState GL.VertexArray GL.$= GL.Disabled
         }

