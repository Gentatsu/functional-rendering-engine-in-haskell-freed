module Display (idle, display, State(..), makeState) where
 
import Graphics.UI.GLUT hiding (Color, Cube, Texture)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Graphics.GLUtil
import Linear 
import System.Exit ( exitWith, ExitCode(ExitSuccess) )

data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

display :: Drawable ->  State -> DisplayCallback
display d state = do 
	clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
	let
		eye' = V3 90 90 90
		dir = V3 0 0 0
		up' = V3 0 1 0
		view = DView {eye=eye', tow=dir, up=up', fov=90, aspectRatio=1.0, znear=0.1, zfar=1500}
		lightPos = Vertex4 0 (-10) 20.0 1.0
		amb' = Color4 0.3 0.3 0.3 1.0
		dif' = Color4 0.3 0.3 0.3 1.0
		spec' = Color4 0.3 0.3 0.3 1.0
		lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Phong} 
	loadIdentity
	render $ Camera view $ Lighting lighting d
	swapBuffers
	--exitWith ExitSuccess
	frames state $~! (+1)
	t0' <- get (t0 state)
	t <- get elapsedTime
	when (t - t0' >= 5000) $ do
		f <- get (frames state)
		let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		let fps = fromIntegral f / seconds
		putStrLn (show fps)
		t0 state $= t
		frames state $= 0

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  
getCamPos :: GLfloat -> (GLfloat, GLfloat)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360



					
