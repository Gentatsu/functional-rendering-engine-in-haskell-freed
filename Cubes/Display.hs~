module Display (idle, display) where
 
import Graphics.UI.GLUT hiding (Color, Cube)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD

display :: IORef GLfloat -> IORef (GLdouble, GLdouble) -> DisplayCallback
display angle pos = do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  loadIdentity
  (x',y') <- get pos
  --translate $ Vector3 x' y' 0
  preservingMatrix $ do
    a <- get angle
    let p = points 5
     	d = getCubes p a
     	camPos = getCamPos (realToFrac a)
     	eye' = Vertex3 ((fst camPos)*10) 20 ((snd camPos)*10) 
     	tow' = Vertex3 0 0 0
     	up'  = Vector3 0 1 0
     	lightPos = Vertex4 1.0 1.0 1.0 0.0
     	amb' = Color4 0.0 0.0 0.0 1.0
     	dif' = Color4 1.0 1.0 1.0 1.0
     	view = DView {eye=eye', tow=tow', up=up', fov=x', aspect_ratio=1, znear=0.1, zfar=1500}
     	lighting = DLighting {pos=lightPos, amb = amb', dif=dif'}
     	c = compile d
    renderDrawable $ Camera view $ Lighting lighting $ Rotate a 0 0 1 $ c
  swapBuffers
 
idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  
getCubes :: [(GLfloat, GLfloat, GLfloat)] -> GLfloat -> Drawable
getCubes l a = Drawables [Translate x y z  $ Color (makeColor x y z 1) (Cube 0.1 0.1 0.1 False) | (x,y,z) <- l]  

getCamPos :: GLdouble -> (GLdouble,GLdouble)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360

