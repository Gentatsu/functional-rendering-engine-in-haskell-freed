{-# OPTIONS_HADDOCK hide #-}

module Graphics.FreeD.Formats.ObjReader
	(
	  loadObj
	)
where
import Graphics.FreeD.Data.Drawable
import Graphics.Rendering.OpenGL.GL	hiding (Vertex, Face) 
import Data.Text (Text)
import Graphics.OBJ.Parser

-- | Loads an obj file's contents in to a Drawable of type DObject [DGeometryVn] Triangles
-- Ignores texture coordinates for now. 
loadObj :: Text -> Drawable
loadObj contents = 
						let obj = tryParseOBJFile contents
						in case obj of
							Left err ->  Blank
							Right x -> DObject (DGeometryVnt $ concatMap getFace $ faces x) Triangles 

getFace :: Face -> [(DVertex, DNormal, DTexCoord)] 
getFace (Face (v1, vn1, t1) (v2, vn2, t2) (v3, vn3, t3)) = [(vTov3 v1, nTon3 vn1, tTot2 t1), (vTov3 v2, nTon3 vn2, tTot2 t2), (vTov3 v3, nTon3 vn3, tTot2 t3)] 

vTov3 :: Vertex -> Vertex3 GLfloat
vTov3 (Vertex a b c) = Vertex3 (realToFrac a) (realToFrac b) (realToFrac c)

nTon3 :: VertexNormal -> Normal3 GLfloat
nTon3 (VertexNormal a b c) = Normal3 (realToFrac a) (realToFrac b) (realToFrac c)

tTot2 :: TextureCoord -> TexCoord2 GLfloat
tTot2 (TextureCoord a b) = TexCoord2 (realToFrac a) (realToFrac b)


