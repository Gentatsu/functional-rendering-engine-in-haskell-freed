module Graphics.FreeD.Rendering.Sphere where
 
import Graphics.UI.GLUT
import Graphics.Rendering.OpenGL.GL
import Graphics.UI.GLUT.Objects

renderSphere ::  GLdouble -> IO ()
renderSphere r = renderObject Solid $ Sphere' r 50 50
