{-# OPTIONS_HADDOCK hide #-}

module Graphics.FreeD.Rendering.Drawable
	(
	  renderDrawable
	, compile 
	)
where
import Graphics.FreeD.Data.Drawable
import Graphics.FreeD.Data.Color
import Graphics.FreeD.Rendering.Color
import Graphics.FreeD.Rendering.Cube
import Graphics.FreeD.Rendering.Sphere
import Graphics.FreeD.Rendering.Bitmap
import Graphics.FreeD.Rendering.State
import Graphics.FreeD.Rendering.Common
import System.Mem.StableName
import Foreign.ForeignPtr
import Data.IORef
import Data.List
import Control.Monad
import Graphics.Rendering.OpenGL	               	(($=), get)
import qualified Graphics.Rendering.OpenGL.GL	        as GL
import qualified Graphics.Rendering.OpenGL.GLU.Errors   as GLU
import qualified Graphics.UI.GLUT		        as GLUT
import Data.Vect.Floating.Base
import System.IO.Unsafe (unsafePerformIO)
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Ptr
import Unsafe.Coerce
import Data.Array.Storable
import Data.Map (toList)
import Graphics.GLUtil hiding (loadTexture, texHeight, texWidth, texData)
import Foreign.Storable (sizeOf)
import Linear 
import Linear.Matrix (M44)

-- | Render a Drawable into the current OpenGL context.
--
--   Assumes that the OpenGL matrix mode is set to @Modelview@
--

renderDrawable
	:: Drawable      -- ^ Drawable to render.
	-> IO ()

renderDrawable drawable
 = do   
	-- Draw the Drawable
        checkErrors "before drawDrawable."
        drawDrawable drawable
        checkErrors "after drawDrawable."

-- | Compile a scene, returning a "Compiled" scene node
compile :: Drawable -> Drawable
compile drawable = unsafePerformIO $
                   do { [dlist] <- GL.genObjectNames 1
	                      ; GL.defineList dlist GL.Compile (drawDrawable drawable)
                      ; return (Compiled dlist)
                      }

concatMapM_        :: (Monad m) => (a -> m [b]) -> [a] -> m [b]
concatMapM_  f xs   =  liftM concat (mapM f xs)

flatten :: DGeometry -> [GL.GLfloat]
flatten (DGeometryV vs) = concatMap (\(GL.Vertex3 v1 v2 v3) -> [v1, v2, v3]) vs
flatten (DGeometryVn vns) = concatMap (\(GL.Vertex3 v1 v2 v3, GL.Normal3 n1 n2 n3) -> [v1, v2, v3, n1, n2, n3]) vns
flatten (DGeometryVt vns) = concatMap (\(GL.Vertex3 v1 v2 v3, GL.TexCoord2 t1 t2) -> [v1, v2, v3, t1, t2]) vns
flatten (DGeometryVnt vns)= concatMap (\(GL.Vertex3 v1 v2 v3, GL.Normal3 n1 n2 n3, GL.TexCoord2 t1 t2) -> [v1, v2, v3, n1, n2, n3, t1, t2]) vns
flatten (DGeometryNv (GL.Normal3 n1 n2 n3) vs)= concatMap (\(GL.Vertex3 v1 v2 v3) -> [v1, v2, v3, n1, n2, n3]) vs
flatten (DGeometryNt nts) = concatMap (\(GL.Normal3 n1 n2 n3, (GL.Vertex3 v1 v2 v3, GL.Vertex3 v4 v5 v6, GL.Vertex3 v7 v8 v9)) -> [v1, v2, v3, n1, n2, n3, v4,v5,v6,n1,n2,n3,v7,v8,v9, n1, n2, n3]) nts
					  
-- | Handles different types of geometry OLD STYLE
{-
process ::  DGeometry  -> IO()
process (DGeometryV vs)     = mapM_ GL.vertex vs
process (DGeometryVn vns)    = mapM_ (\(v,n) -> GL.normal n >> GL.vertex v) vns 
process (DGeometryVt vns)    = mapM_ (\(v,t) -> GL.texCoord t >> GL.vertex v) vns 
process (DGeometryVnt vns)    = mapM_ (\(v,n,t) -> GL.normal n >> GL.texCoord t >> GL.vertex v) vns 
process (DGeometryNv n vs)   = GL.normal n >> mapM_ GL.vertex vs
process (DGeometryNt nts)    = mapM_ tri nts
                             where
                                tri (n,(v1,v2,v3)) = GL.normal n >> GL.vertex v1 >> GL.vertex v2 >> GL.vertex v3
-}
                                

renderNv :: GL.BufferObject -> GL.PrimitiveMode  -> GLUT.NumArrayIndices  -> IO ()
renderNv buff mode size = do
    let stride = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 6
        offset = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 3
        vxDesc = GL.VertexArrayDescriptor 3 GL.Float stride $ offset0
        nDesc = GL.VertexArrayDescriptor 3 GL.Float stride $ (offsetPtr offset)
    GL.bindBuffer GL.ArrayBuffer $= Just buff
    GL.arrayPointer GL.VertexArray $= vxDesc
    GL.arrayPointer GL.NormalArray $= nDesc
    GL.clientState GL.VertexArray $= GL.Enabled
    GL.clientState GL.NormalArray $= GL.Enabled
    GL.drawArrays mode 0 $ size `div` 6
    GL.bindBuffer GL.ArrayBuffer $= Nothing


-- | Renders the drawable
drawDrawable :: Drawable -> IO ()	  
drawDrawable  drawable
 = {-# SCC "drawComponent" #-}
   case drawable of

	-- nothin'
	Blank
	 -> 	return () 
	 
	DObject geom mode
		->	do	
		let flat = flatten geom
		let size = fromIntegral $ length flat
		buff <- makeBuffer GL.ArrayBuffer flat
	 	case geom of 

			DGeometryV _
				->	do	
					let stride = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 3
					let	vxDesc = GL.VertexArrayDescriptor 3 GL.Float stride $ offset0
					GL.bindBuffer GL.ArrayBuffer $= Just buff
					GL.arrayPointer GL.VertexArray $= vxDesc
					GL.clientState GL.VertexArray $= GL.Enabled
					GL.drawArrays mode 0 $ size `div` 3
					GL.bindBuffer GL.ArrayBuffer $= Nothing
				
		 	DGeometryVn _ 
		 		->	renderNv buff mode size 				

			DGeometryVnt _
				-> do
					let 
						stride = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 8
						normalOffset = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 3
						texOffset = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 6
						vxDesc = GL.VertexArrayDescriptor 3 GL.Float stride $ offset0
						nDesc = GL.VertexArrayDescriptor 3 GL.Float stride $ (offsetPtr normalOffset)
						tDesc = GL.VertexArrayDescriptor 2 GL.Float stride $ (offsetPtr texOffset)
					GL.bindBuffer GL.ArrayBuffer $= Just buff
					GL.arrayPointer GL.VertexArray $= vxDesc
					GL.arrayPointer GL.NormalArray $= nDesc
					GL.arrayPointer GL.TextureCoordArray  $= tDesc
					GL.clientState GL.VertexArray $= GL.Enabled
					GL.clientState GL.NormalArray $= GL.Enabled
					GL.clientState GL.TextureCoordArray  $= GL.Enabled
					GL.drawArrays mode 0 $ size `div` 8
					GL.bindBuffer GL.ArrayBuffer $= Nothing

			DGeometryVt _
				->	do
					let
						stride = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 5
						offset = fromIntegral $ sizeOf (undefined::GL.GLfloat) * 3
						vxDesc = GL.VertexArrayDescriptor 3 GL.Float stride $ offset0
						tDesc = GL.VertexArrayDescriptor 2 GL.Float stride $ (offsetPtr offset)
					GL.bindBuffer GL.ArrayBuffer $= Just buff
					GL.arrayPointer GL.VertexArray $= vxDesc
					GL.arrayPointer GL.TextureCoordArray  $= tDesc
					GL.clientState GL.VertexArray $= GL.Enabled
					GL.clientState GL.TextureCoordArray  $= GL.Enabled
					GL.drawArrays mode 0 $ size `div` 6
					GL.bindBuffer GL.ArrayBuffer $= Nothing

			DGeometryNv _ _ 
				->	renderNv buff mode size

			DGeometryNt _
				-> renderNv buff mode size

	-- Sphere
 	Sphere radius	
	 -> renderSphere radius
	 
	-- Cube
	Cube width height depth wireframe
	 ->  
	 	case wireframe of
	 	False	-> drawBox --renderCube width height depth 
		True	-> cubeFrame width
		
	-- colors with GLfloat components.
	Color col p
	 ->  do	oldColor 	 <- get GL.currentColor

		let RGBA r g b a  = col
		GL.currentColor	 $= GL.Color4 (r) (g) (b) (a)
		drawDrawable  p
		GL.currentColor	 $= oldColor		

  -- Translation --------------------------	 
	Translate tx ty	tz p
	 -> GL.preservingMatrix 
	  $ do  GL.translate (GL.Vector3 tx ty tz)
		drawDrawable p
		
	Rotate deg xAxis yAxis zAxis p
	 -> GL.preservingMatrix
	  $ do	mat <- toGLMatrix $ getMatrix (axisAngle (V3 xAxis yAxis zAxis) r) 
		GL.multMatrix mat
		drawDrawable p 
		where r = (pi / 180) * deg
	
	Camera view  p
		->	do 
		GL.matrixMode $= GL.Projection
		GL.loadIdentity
		pMatrix <- toGLMatrix $ perspective ((pi / 180) * fov view) (aspect_ratio view) (znear view) (zfar view)
		GL.multMatrix  pMatrix
		GL.matrixMode $= GLUT.Modelview 0
		GL.loadIdentity
		mMatrix <- toGLMatrix $ lookAt (eye view) (tow view) (up view)
		GL.preservingMatrix $ do
			GL.multMatrix mMatrix
			drawDrawable p
	
	Lighting lighting sm p
		->	do
		GL.depthMask  $= GL.Enabled
		GL.depthFunc  $= Just GL.Less
		--GL.colorMaterial $= Just (GL.FrontAndBack, GL.Diffuse)
		GL.lighting $= GL.Enabled
		GL.shadeModel $= sm
		GL.light (GL.Light 0) $= GL.Enabled
		GL.position (GL.Light 0) $= pos lighting
		GL.ambient (GL.Light 0)  $= amb lighting
		GL.diffuse (GL.Light 0)  $= dif lighting
		GL.specular (GL.Light 0) $= spec lighting
		drawDrawable p
		GL.lighting $= GL.Disabled

	Material m p
		->	do
			GL.materialAmbient  GL.FrontAndBack $= matAmb m
			GL.materialDiffuse  GL.FrontAndBack $= matDif m
			GL.materialSpecular GL.FrontAndBack $= matSpec m
			drawDrawable p
		
	Compiled dlist
		-> GL.callList dlist
	
	-- |Texture
	Texture tex p
	 -> do	
	 
		-- Set up wrap and filtering mode
		GL.textureWrapMode GL.Texture2D GL.S $= (GL.Repeated, GL.Repeat)
		GL.textureWrapMode GL.Texture2D GL.T $= (GL.Repeated, GL.Repeat)
		GL.textureFilter   GL.Texture2D      $= ((GL.Nearest, Nothing), GL.Nearest)
		
		-- Enable texturing
		GL.texture GL.Texture2D $= GL.Enabled
		GL.textureFunction      $= GL.Combine
		
		-- Set current texture
		GL.textureBinding GL.Texture2D $= Just tex
		
		-- Draw textured Drawable
		drawDrawable p
		-- Disable texturing
		GL.texture GL.Texture2D $= GL.Disabled

    -- Free uncachable texture objects.

	TextureOff 
		->	do	GL.texture GL.Texture2D $= GL.Disabled
	
	Drawables ps
	 -> mapM_ (drawDrawable) ps

-- Convert a Unit Quaternion to a 4x4 Column-Major OpenGL Matrix
getMatrix :: Num a => Quaternion GL.GLfloat -> M44 GL.GLfloat
getMatrix (Quaternion w (V3 x y z))
    = V4 (V4 (1 - 2*(yy+zz))  (2*(xy-zw))     (2*(yw+xz))       0)
           (V4 (2*(xy+zw))      (1 - 2*(xx+zz)) (2*(yz-xw))       0)
           (V4 (2*(xz-yw))      (2*(yz+xw))     (1 - 2*(xx + yy)) 0)
           (V4 0                0               0                 1)
      where           
          xx      = x * x
          xy      = x * y
          xz      = x * z
          xw      = x * w

          yy      = y * y
          yz      = y * z
          yw      = y * w

          zz      = z * z
          zw      = z * w
          ww      = w * w

toGLMatrix :: M44 GL.GLfloat -> IO (GL.GLmatrix GL.GLfloat)
toGLMatrix (V4 (V4 a11 a12 a13 a14)
                 (V4 a21 a22 a23 a24)
                 (V4 a31 a32 a33 a34)
                 (V4 a41 a42 a43 a44))
    = GL.newMatrix GL.RowMajor [ realToFrac a11, realToFrac a12, realToFrac a13, realToFrac a14
                               , realToFrac a21, realToFrac a22, realToFrac a23, realToFrac a24
                               , realToFrac a31, realToFrac a32, realToFrac a33, realToFrac a34
                               , realToFrac a41, realToFrac a42, realToFrac a43, realToFrac a44
                               ]

-- Errors ---------------------------------------------------------------------
checkErrors :: String -> IO ()
checkErrors place
 = do   errors          <- get $ GLU.errors
        when (not $ null errors)
         $ mapM_ (handleError place) errors

handleError :: String -> GLU.Error -> IO ()
handleError place err
 = case err of
    GLU.Error GLU.StackOverflow _
     -> error $ unlines 
      [ "FreeD / OpenGL Stack Overflow " ++ show place
      , "  This program uses the FreeD vector graphics library, which tried to"
      , "  draw a Drawable using more nested transforms (Translate/Rotate/Scale)"
      , "  than your OpenGL implementation supports. The OpenGL spec requires"
      , "  all implementations to have a transform stack depth of at least 32,"
      , "  and FreeD tries not to push the stack when it doesn't have to, but"
      , "  that still wasn't enough."
      , ""
      , "  You should complain to your harware vendor that they don't provide"
      , "  a better way to handle this situation at the OpenGL API level."
      , ""
      , "  To make this program work you'll need to reduce the number of nested"
      , "  transforms used when defining the Drawable given to FreeD. Sorry." ]

    -- Issue #32: Spurious "Invalid Operation" errors under Windows 7 64-bit.
    --   When using GLUT under Windows 7 it complains about InvalidOperation, 
    --   but doesn't provide any other details. All the examples look ok, so 
    --   we're just ignoring the error for now.
    GLU.Error GLU.InvalidOperation _
     -> return ()
    _ 
     -> error $ unlines 
     [  "FreeD / OpenGL Internal Error " ++ show place
     ,  "  Please report this on haskell-FreeD@googlegroups.com."
     ,  show err ]




