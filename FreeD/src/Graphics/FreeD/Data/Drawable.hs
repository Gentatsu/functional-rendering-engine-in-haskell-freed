{-# OPTIONS_HADDOCK hide #-}
{-# OPTIONS -fno-warn-orphans #-}

-- | Data types for representing Drawables.
module Graphics.FreeD.Data.Drawable
	( Drawable(..)
	, DView(..)
	, DLighting(..)
	, DGeometry(..)
	, BitmapData(..)
	, DVertex(..)
	, DNormal(..)
	, DTexCoord(..)
	, Material(..)
	, loadTex
  )
where
import qualified Graphics.FreeD.Data.Color as C
import Graphics.FreeD.Rendering.Bitmap
import Graphics.FreeD.Rendering.Common
import Graphics.FreeD.Rendering.State
import Data.Monoid
import Data.ByteString
import Data.Data
import Data.Word
import System.IO.Unsafe
import Graphics.UI.GLUT hiding (Texture)
import Foreign.ForeignPtr
import System.Mem.StableName
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Ptr
import qualified Data.ByteString.Unsafe as BSU
import Prelude hiding (map)
import Graphics.GLUtil
import Linear (V3(..))
-- | Data type for camera view
data DView = DView 
                 { 
                  eye    :: V3 GLfloat
                 , tow    :: V3 GLfloat
                 , up    :: V3 GLfloat
                 , fov  :: GLfloat
                 , aspect_ratio :: GLfloat
                 , znear :: GLfloat
                 , zfar  :: GLfloat
                 }
                 deriving (Show, Eq)     

-- | Data type for lighting properties
data DLighting = DLighting
									{
										pos :: Vertex4 GLfloat
									, amb :: Color4 GLfloat
									, dif :: Color4 GLfloat
									, spec :: Color4 GLfloat
									} 
									deriving (Show, Eq)

type DVertex = Vertex3 GLfloat
type DNormal = Normal3 GLfloat
type DVector = Vector3 GLfloat
type DTexCoord = TexCoord2 GLfloat

-- | Data type for different types of geometry which can be rendered. 									
data DGeometry = DGeometryV [DVertex]
            | DGeometryVn [(DVertex, DNormal)]
            | DGeometryNv DNormal [DVertex]
            | DGeometryNt [(DNormal, (DVertex, DVertex, DVertex))]
            | DGeometryVnt [(DVertex, DNormal, DTexCoord)]
            | DGeometryVt [(DVertex, DTexCoord)]
			deriving (Show, Eq)


type WireFrame = Bool

data Material = Mat { matAmb :: Color4 GLfloat, matDif :: Color4 GLfloat, matSpec :: Color4 GLfloat} 
	deriving (Show, Eq, Typeable)

-- | A 3D Drawable
data Drawable
	-- Primitives -------------------------------------

	-- | A blank Drawable, with nothing in it.
	= Blank
	
	-- | Geometry with the primitive drawing type (Lines, Triangles, Triangle_strips, etc)
	| DObject !DGeometry PrimitiveMode

	-- | A Sphere with given radius
	| Sphere !GLdouble
	
	-- | A Cube with width and height and depth, boolean to determine wireframe or not. 
	| Cube !GLfloat !GLfloat !GLfloat	!WireFrame

	-- | A Toroid with two radii
	| Toroid	!GLfloat !GLfloat
	
	-- | A Cylinder with given radius and height
	| Cylinder !GLfloat !GLfloat
	
	-- Color ------------------------------------------
	-- | A Drawable drawn with this color.
	| Color	!	C.Color  !Drawable

	-- Transforms -------------------------------------
	-- | A Drawable translated by the given x,y and z coordinates.
	| Translate	!GLfloat !GLfloat	!GLfloat !Drawable

	-- | A Drawable rotated clockwise by the given angle, and axis (in degrees).
	| Rotate !GLfloat	!GLfloat !GLfloat !GLfloat !Drawable

	-- | A Drawable scaled by the given x, y and z factors.
	| Scale		!GLfloat	!GLfloat	!GLfloat !Drawable
	
	{- | Camera, to do perspective, can have multiple, but at the moment assumes only one
	     Given eye, facing towards, and up vector
	 		 For perspective: angle, aspect ratio, znear, zfar -}
	| Camera !DView !Drawable
	
	-- | Lighting, given position, ambient and diffuse aspects
	| Lighting !DLighting !ShadingModel !Drawable
	
	-- | For already compiled nodes. 
	| Compiled !DisplayList
	
	-- | Texture to drawn
	| Texture	!TextureObject !Drawable

	| TextureOff 

	| Material Material Drawable 

	-- More Drawables ----------------------------------
	-- | A Drawable consisting of several others.
	| Drawables	[Drawable]
	deriving (Show, Eq, Typeable)
	
	
-- Instances ------------------------------------------------------------------
instance Monoid Drawable where
	mempty		= Blank
	mappend a b	= Drawables [a, b]
	mconcat		= Drawables


loadTex :: FilePath -> IO (Either String TextureObject)
loadTex filename = do
										t <- readTexture filename
										textureWrapMode Texture2D S $= (Repeated, Repeat)
										textureWrapMode Texture2D T $= (Repeated, Repeat)
										textureFilter   Texture2D      $= ((Nearest, Nothing), Nearest) 
										return t


