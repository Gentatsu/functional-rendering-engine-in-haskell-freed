#version 150

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;


out vec4 eyeCordFs;
out vec4 eyeNormalFs;
out vec2 fTexCoord;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat4 uNormalMatrix;


void main()
{   
    vec4 eyeNorm = normalize(uNormalMatrix * vec4(vNormal, 0.0));
    vec4 eyeCord= uModelViewMatrix * vec4(vPosition, 1.0);

    eyeCordFs = eyeCord;
    eyeNormalFs = eyeNorm;
    fTexCoord = vTexCoord;

    gl_Position = uProjectionMatrix * uModelViewMatrix * vec4( vPosition,1.0);
}
