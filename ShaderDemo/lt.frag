#version 150

in vec4 eyeCordFs;
in vec4 eyeNormalFs;
in vec2 fTexCoord;

out vec4 fColour;

uniform vec4 uLightPos;
uniform vec4 uLightAmb;
uniform vec4 uLightDif;
uniform vec4 uLightSpec;

uniform sampler2D uTexUnit;

void main()
{
    vec4 s = normalize(uLightPos - eyeCordFs) ;
    vec4 r = reflect(-s,eyeNormalFs);
    vec4 v = normalize(-eyeCordFs);
    vec4 texColour = texture(uTexUnit, fTexCoord);
    float spec = max( dot(v,r),0.0 );
    float diff = max(dot(eyeNormalFs,s),0.0);

    vec4 diffColour = diff * uLightDif * texColour;
    vec4 specColour = pow(spec,3) * uLightSpec;
    vec4 ambientColour = uLightAmb * texColour;

    fColour =  diffColour + 0.5 * specColour + ambientColour; 
}
