module Display (idle, display, initi) where
 
import Graphics.UI.GLUT hiding (Color, Cube, lookAt, Texture, ShadingModel, Flat, Sphere)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Data.Text hiding (transpose, map, length)
import Graphics.GLUtil
import Foreign.Storable (sizeOf)
import Data.Map ((!), keys)
import Linear 

initi :: IORef GLfloat -> IORef (GLfloat, GLfloat) -> IORef [TextureObject] -> IO Drawable
initi angle posi tex= do  
	loadIdentity
	(x',y') <- get posi
	a <- get angle
	[t, t2] <- get tex
	objContents <- readFile "suzanne.obj"
	--cubeContents <- readFile "cube.obj"
	let 
		p = points 5
		camPos = getCamPos (realToFrac a)
		eye' = V3 ((fst camPos)*10) 20 ((snd camPos)*10) 
		tow' = V3 0 0 0
		up'  = V3 0 1 0
		lightPos = Vertex4 1.0 1.0 1.0 0.0
		amb' = Color4 0.5 0.5 0.5 1.0
		dif' = Color4 0.5 0.5 0.5 1.0
		spec' = Color4 0.0 0.0 0.0 0.0
		view = DView {eye=eye', tow=tow', up=up', fov=x', aspectRatio=1.0, znear=0.1, zfar=1500}
		lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Phong} 
		c2 = {-Texture t2 $-} loadObj $ pack objContents
		d = Texture t2 $ getCubes p a c2
		drawables = Drawables [d, c2]
		mat =  Mat {matAmb= Color4 0.8 0.8 0.8 1, matDif = Color4 0.8 0.8 0.8 1, matSpec = Color4 0.1 0.1 0.1 0.5}
	cd <- compile $ Lighting lighting $ Material mat {-Rotate a 0 0 1-} drawables
	return cd

display :: IORef GLfloat -> IORef (GLfloat, GLfloat) -> IORef [TextureObject] -> Drawable -> DisplayCallback
display angle posi tex cd= do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  loadIdentity
  (x',y') <- get posi
  preservingMatrix $ do
		a <- get angle
		[t, t2] <- get tex
		objContents <- readFile "suzanne.obj"
		let 
			p = points 5
			camPos = getCamPos (realToFrac a)
			eye' = V3 x' x' x' --eye' = V3 ((fst camPos)*10) 20 ((snd camPos)*10) 
			tow' = V3 0 0 0
			up'  = V3 0 1 0
			lightPos = Vertex4 1.0 1.0 1.0 0.0
			amb' = Color4 1.0 1.0 1.0 1.0
			dif' = Color4 1.0 1.0 0.0 1.0
			spec' = Color4 1.0 1.0 1.0 1.0
			view = DView {eye=eye', tow=tow', up=up', fov=45, aspectRatio=1.0, znear=0.1, zfar=1500}
			lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Phong} 
			--c = loadObj $ pack objContents
			c2 = Texture t2 $ loadObj $ pack objContents
			d = Texture t $ getCubes p a c2
			drawables = Drawables [d, c2]
			mat =  Mat {matAmb= Color4 1 0.0 0.0 1, matDif = Color4 1 1 1 1, matSpec = Color4 1 1 1 1}
		render  $ Camera view $ Lighting lighting $ Material mat $ Rotate a 0 0 1 drawables
		--render $ Camera view $ Rotate a 0 0 1 cd
		--renderCompiled cd2
  swapBuffers

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  
getCubes :: [(GLfloat, GLfloat, GLfloat)] -> GLfloat -> Drawable -> Drawable
getCubes l a d = Drawables [Translate (x*5)(y*5) (z*5)  $ Color (Color4 x y z 1) (Cube 1 1 1) | (x,y,z) <- l]  

getCamPos :: GLfloat -> (GLfloat,GLfloat)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360

