-- | Data types for representing state.
module Graphics.FreeD.Data.GLState
	( 
	 GLState(..)
	, initGLState
  )
where
import Graphics.UI.GLUT (GLfloat, Color4(..), TextureObject)
import Graphics.GLUtil
import Linear.Matrix
import Graphics.FreeD.Data.Matrix
import Graphics.FreeD.Data.Misc
import Data.IORef
import Graphics.FreeD.Shaders.ShaderProgramType
import System.IO.Unsafe (unsafePerformIO)
-- | Data type for the current state as it is traversed when rendering. 
data GLState a = GLState 
	{
		mMatrix ::  M44 a -- ^ Model Matrix (for transformations).
	 ,vMatrix :: Maybe (M44 a) -- ^ If a view Matrix is set (for camera settings).
	 ,pMatrix :: Maybe (M44 a) -- ^ If a projection matrix is set (for perspective settings).
	 ,lighting :: Maybe DLighting -- ^ If lighting properties are set.
	 ,texture :: Maybe TextureObject -- ^ If a texture has been set.
	 ,sProgram :: Maybe ShaderProgram -- ^ If a custom shader program is used.
	 ,shaderProgramType :: ShaderProgramType -- ^ Record syntax of default shaders.
	 ,material :: Maybe (Material a) -- ^ Material properties.
	 ,color :: Color4 a -- ^ Color.
	}
	deriving (Show, Ord, Eq)

-- | If it has not been initialised, then initialises the shaderProgramType, else it simply returns the IORef to the initialised ShaderProgramType.
getDefaultShadersRef :: IORef ShaderProgramType
{-# NOINLINE getDefaultShadersRef #-}
getDefaultShadersRef = unsafePerformIO $ newIORef $ unsafePerformIO initSPT 

-- | Returns an initialised GLState objection, where each matrix is set to the identity, the color is white, and everything else is Nothing.
initGLState :: (Num a) => IO (GLState a)
initGLState = do
								-- Gets the default shaders from an IORef. 
								spt <- readIORef getDefaultShadersRef
								return GLState {mMatrix=p, vMatrix=Nothing, pMatrix=Nothing, lighting= Nothing, texture=Nothing, sProgram=Nothing, material=Nothing, color=col, shaderProgramType=spt}
								where 
									p = getIdentity
									col = Color4 1 1 1 1
