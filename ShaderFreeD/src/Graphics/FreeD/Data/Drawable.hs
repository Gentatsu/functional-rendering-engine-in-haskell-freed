-- | Data type constructors for representing a Drawable.
module Graphics.FreeD.Data.Drawable
	( Drawable(..)
  )
where
import Graphics.FreeD.Data.Misc
import Graphics.FreeD.Data.GLState
import Data.Monoid
import Graphics.UI.GLUT hiding (Texture, lookAt, lighting, texture, ShadingModel, Color, Cube, Sphere)
import Graphics.GLUtil (ShaderProgram)
import Data.Typeable
import Data.Semigroup

-- | Drawable data type (Scene Graph) 
data Drawable a
	-- Primitives -------------------------------------

	-- | A blank Drawable, with nothing in it.
	= Blank

	-- | Geometry with the primitive drawing type (Lines, Triangles, Triangle_strips, etc)
	| DObject (DGeometry a) PrimitiveMode

	-- | A Sphere with given radius
	| Sphere a
	
	-- | A Cube with given width, height and depth. 
	| Cube a a a	

	-- | A Drawable drawn with this color.
	| Color (Color4 a) (Drawable a)
	
	-- | A Drawable drawn with these material properties when lighting is used.
	| Material	(Material a)  (Drawable a)

	-- Transformationss -------------------------------------
	
	-- | A Drawable translated by the given x,y and z coordinates.
	| Translate	a a	a (Drawable a)

	-- | A Drawable rotated clockwise by the given angle (in degrees) , and axis.
	| Rotate a a a a (Drawable a)

	-- | A Drawable scaled by the given x, y and z factors.
	| Scale	a	a	a (Drawable a)
	
	{- | Camera, to do perspective, can have multiple, but at the moment assumes only one
	     Given eye, facing towards, and up vector.
	 		 For perspective: angle, aspect ratio, znear, zfar. -}
	| Camera (DView a) (Drawable a)
	
	-- | Lighting, given position, ambient,diffuse, and specular properties.
	| Lighting DLighting (Drawable a)
	
	-- | Texture to be used for Drawable (if texture coordinates are given).
	| Texture	TextureObject (Drawable a)

	-- | To turn off the current Texture.
	| TextureOff (Drawable a)

	-- | A Drawable consisting of several others.
	| Drawables	[(Drawable a)]

	-- | A compiled drawable given each property to draw from.
	| CompiledDrawable 
		{
			 pMode :: !PrimitiveMode -- ^ Primitive Mode to draw from.
			,vboSize :: !NumArrayIndices -- ^ Size of flattened vertex array.
			,divSize :: !NumArrayIndices -- ^ Values per vertex in vertex array.
			,vao :: !VertexArrayObject -- ^ Vertex Array Object 
			,glState :: !(GLState a) -- ^ GLState carried around. 
		}

	-- | A Shader Program to use when rendering.
	| Shader !ShaderProgram (Drawable a)

	-- | A Handler for events regarding transformations (NOT IMPLEMENTED)
	| Handler (Drawable a)
	
	deriving (Typeable, Show)

-- Instances ------------------------------------------------------------------
-- | Monoid Instance 
instance Semigroup t => Monoid (Drawable t) where
	mempty		= Blank 
	Blank `mappend` a = a
	a `mappend` Blank = a
	mappend a b	= Drawables [a, b] -- Puts two Drawable objects in to a list
	mconcat		= Drawables -- Appends all Drawables and sticks them in one. 


instance Functor Drawable where
	fmap _ Blank = Blank
	fmap f (Sphere r) = Sphere $ f r
	fmap f (Cube x y z) = Cube (f x) (f y) (f z)
	fmap f (Color c d) = Color (fmap f c) (fmap f d)
	--fmap f (Material m d) = Material (fmap f m) d
	fmap f (Translate x y z d) = Translate (f x) (f y) (f z) (fmap f d)
	fmap f (Scale x y z d) = Scale (f x) (f y) (f z) (fmap f d)
	fmap f d = fmap f d

	
