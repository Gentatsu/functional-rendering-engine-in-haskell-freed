-- | Matrix data types and operations.
module Graphics.FreeD.Data.Matrix
	(  getMatrix
	, getMVP
	, getMV
	, getP
	, translationMat
	, scaleMat
	, getIdentity
	, m44_to_m33
	, matToList
  )
where
import Data.Monoid
import Graphics.UI.GLUT hiding (Texture, lookAt, lighting, texture)
import Graphics.GLUtil
import Linear hiding (adjoint)
import Linear.Matrix hiding (adjoint)
import Graphics.GLUtil.Camera3D


-- | Convert a Quaternion to a 4x4 Linear Matrix type.
-- Taken from my supervisor, David Duke.
getMatrix :: Num a => Quaternion a -> M44 a
getMatrix (Quaternion w (V3 x y z))
    = V4 (V4 (1 - 2*(yy+zz))  (2*(xy-zw))     (2*(yw+xz))       0)
           (V4 (2*(xy+zw))      (1 - 2*(xx+zz)) (2*(yz-xw))       0)
           (V4 (2*(xz-yw))      (2*(yz+xw))     (1 - 2*(xx + yy)) 0)
           (V4 0                0               0                 1)
      where           
          xx      = x * x
          xy      = x * y
          xz      = x * z
          xw      = x * w

          yy      = y * y
          yz      = y * z
          yw      = y * w

          zz      = z * z
          zw      = z * w
          ww      = w * w

-- | Constructs a translation matrix given x,y,z.
translationMat :: (Num a) => a -> a -> a -> M44 a
translationMat tx ty tz = V4 (V4 1 0 0 tx)
														 (V4 0 1 0 ty)
														 (V4 0 0 1 tz)
														 (V4 0 0 0 1)

-- | Constructs a scale matrix given x,y,z.
scaleMat :: (Num a) =>  a -> a -> a -> M44 a
scaleMat tx ty tz = V4 (V4 tx 0 0 0)
											 (V4 0 ty 0 0)
											 (V4 0 0 tz 0)
											 (V4 0 0 0  1)
														 
-- | Gets current ModelViewProjection matrix on stack.							 
getMVP :: IO (M44 GLfloat)
getMVP = 	do
	-- Gets modelview and projection matrices as M44.
	mvm <- getMV
	pm <- getP 
	-- Multiplies them in order modelview matrix * projection matrix.
	let mvmp = mvm !*! pm
	return mvmp
										
-- | Gets current ModelView matrix on stack.
getMV :: IO (M44 GLfloat)
getMV = 	do
	-- Gets current ModelView Matrix on stack.
	mvMatrix <- get ((matrix $ Just $ Modelview 0)::StateVar(GLmatrix GLfloat))
	-- Gets the components of the matrix as [GLfloat].
	mv <- getMatrixComponents RowMajor mvMatrix
	-- Converts [GLfloat] to M44.
	let mvm = getMat mv
	return mvm

-- | Gets current Project matrix on stack.
getP :: IO (M44 GLfloat)
getP = 	do
	-- Gets current Projection matrix on stack.
	pMatrix <- get ((matrix $ Just Projection)::StateVar(GLmatrix GLfloat))
	-- Gets the components of the matrix as [GLfloat].
	pm <-	getMatrixComponents RowMajor pMatrix
	-- Converts [GLfloat] to M44.
	let pM = getMat pm 
	return pM

-- | Converts list of GLfloat values in to a 4x4 Linear Matrix.										
getMat :: [GLfloat] -> M44 GLfloat
getMat (a11:a12:a13:a14: 
				a21:a22:a23:a24:
				a31:a32:a33:a34:
				a41:a42:a43:a44:_) = V4 (V4 a11 a12 a13 a14)
                 (V4 a21 a22 a23 a24)
                 (V4 a31 a32 a33 a34)
                 (V4 a41 a42 a43 a44)
              
-- | Converts M44 to list of list									
matToList :: M44 a -> [[a]]
matToList (V4 (V4 a11 a12 a13 a14)
       (V4 a21 a22 a23 a24)
       (V4 a31 a32 a33 a34)
       (V4 a41 a42 a43 a44)) =
       [[a11, a12, a13, a14],
        [a21, a22, a23, a24],
        [a31, a32, a33, a34],
        [a41, a42, a43, a44]] 

-- | Identity matrix. 
getIdentity :: (Num a) => M44 a
getIdentity = V4 (V4 1 0 0 0) 
								 (V4 0 1 0 0)
								 (V4 0 0 1 0)
								 (V4 0 0 0 1)

-- | Convert a 4x4 Matrix to 3x3 by chopping off 4th row and column.
m44_to_m33 :: M44 a -> M33 a
m44_to_m33 (V4 (V4 a1 b1 c1 d1) (V4 a2 b2 c2 d2) (V4 a3 b3 c3 d3) (V4 a4 b4 c4 d4)) = V3 (V3 a1 b1 c1) (V3 a2 b2 c2) (V3 a3 b3 c3)

