-- | Misc data types and functions.
module Graphics.FreeD.Data.Misc
	( 
	 DView(..)
	, DLighting(..)
	, DGeometry(..)
	, DVertex(..)
	, DNormal(..)
	, DTexCoord(..)
	, ShadingModel(..)
	, Material(..)
	, loadTex
  )
where
import Graphics.FreeD.Data.Matrix
import Graphics.UI.GLUT hiding (Texture, lookAt, lighting, texture, ShadingModel)
import Graphics.GLUtil
import Linear
import Data.Typeable

-- | Data type for material properties given ambient, diffuse, and specular.
data Material a = Mat { matAmb :: Color4 a, matDif :: Color4 a, matSpec :: Color4 a} 
	deriving (Show, Eq, Typeable, Ord)

-- | Data type for camera view. 
data DView a = DView 
                 { 
                  eye    :: (V3 a) -- ^ Position of camera
                 , tow    :: (V3 a) -- ^ Center pivot position or what position the camera is looking towards.
                 , up    :: (V3 a) -- ^ Up vector of camera (What direction is up for the camera).
                 , fov  :: a -- ^ Field of view.
                 , aspectRatio :: a -- ^ Aspect ratio of window.
                 , znear :: a -- ^ Minimum z value.
                 , zfar  :: a -- ^ Maximum z value.
                 }
                 deriving (Show, Eq, Ord)

-- | Data type for different shading models for lighting.
data ShadingModel = Flat | Gourad | Phong
	deriving (Show, Eq, Ord)

-- | Data type for lighting properties
data DLighting = DLighting
									{
										pos :: !(Vertex4 GLfloat) -- ^ Position of light.
									, amb :: !(Color4 GLfloat) -- ^ Ambient property.
									, dif :: !(Color4 GLfloat) -- ^ Diffuse property.
									, spec :: !(Color4 GLfloat) -- ^ Specular property.
									, shadingModel :: !ShadingModel -- ^ Shading model used.
									} 
									deriving (Show, Eq, Ord)

-- Type values for convenience.

-- | Vertex3 constructor with GLfloat
type DVertex = Vertex3 GLfloat
-- | Normal4 constructor with GLfloat.
type DNormal = Normal3 GLfloat
-- | Vector3 constructor with GLfloat.
type DVector = Vector3 GLfloat
-- | TexCoord2 constructor with GLfloat.
type DTexCoord = TexCoord2 GLfloat

-- | Data type for different types of geometry which can be rendered. 									
data DGeometry a = DGeometryV [Vertex3 a] -- ^ List of vertices
            | DGeometryVn [(Vertex3 a, Normal3 a)] -- ^ List of pair of vertices and normals.
            | DGeometryNv (Normal3 a) [Vertex3 a] -- ^ One normal for a list of vertices.
            | DGeometryNt [(Normal3 a, (Vertex3 a, Vertex3 a, Vertex3 a))] -- ^ List of one normal per triangle (triple of vertices).
            | DGeometryVnt [(Vertex3 a, Normal3 a, TexCoord2 a)] -- ^ List of tripe of vertices, normals and texture coordinates.
            | DGeometryVt [(Vertex3 a, TexCoord2 a)] -- ^ List of pair of vertices and texture coordinates.
			deriving (Show, Eq, Ord)


-- | Convienience function to load textures and set properties. Relies of GLUtil's readTexture function.
loadTex :: FilePath -> IO (Either String TextureObject)
loadTex filename = do
	-- Read texture in from GLUtil package.
	t <- readTexture filename
	-- Set wrapping to repeat, so texture just repeats itself if the object is too large.
	textureWrapMode Texture2D S $= (Repeated, Repeat)
	textureWrapMode Texture2D T $= (Repeated, Repeat)
	-- Use nearest value to approximate
	textureFilter   Texture2D      $= ((Nearest, Nothing), Nearest) 
	return t

