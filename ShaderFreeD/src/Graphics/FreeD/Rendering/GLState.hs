-- | Functions for altering the GLState

module Graphics.FreeD.Rendering.GLState
	(
	  translate,
	  rotate,
	  cam,
	  scale
	)
	
where
import Graphics.FreeD.Data.GLState
import Graphics.FreeD.Data.Matrix
import Graphics.FreeD.Data.Misc
import Graphics.Rendering.OpenGL	               	(($=), get)
import qualified Graphics.Rendering.OpenGL.GL	        as GL
import qualified Graphics.Rendering.OpenGL.GLU.Errors   as GLU
import qualified Graphics.UI.GLUT		        as GLUT
import Graphics.GLUtil hiding (loadTexture, texHeight, texWidth, texData)
import Linear hiding (rotate)
import Graphics.GLUtil.Camera3D hiding (Camera)
import Data.Map ((!), keys, toList)


-- | Performs scaling on matrix in state given x,y,z variables.
scale :: (Num a) => a -> a -> a -> (GLState a) -> (GLState a)
--  Constructs scale matrix, multiplies with current model matrix in state.
scale x y z state =  let newM = mMatrix state !*! scaleMat x y z
										 in state { mMatrix=newM}
												
-- | Performs a translation on matrix in state given x,y,z variables.
translate :: (Num a) => a -> a -> a -> (GLState a) -> (GLState a)
--  Constructs translation matrix, multiplies with current model matrix in state.
translate x y z state = let newM = mMatrix state !*! translationMat x y z
												in state { mMatrix=newM}

-- | Performs a rotation on matrix in state, given angle, x,y,z axis varibles.
rotate :: (Num a, Epsilon a, Floating a) => a -> a -> a -> a -> (GLState a) -> (GLState a)
rotate d x y z state = state { mMatrix=newM}
											 where
											 --  Constructs rotation matrix, multiplies with current model matrix in state.
											 --  Does this using quaternions. 
											 newM = mMatrix state !*!  getMatrix (axisAngle (V3 x y z) r)
											 -- Degrees converted to radians.
											 r = rad * d

-- | Performs a projection matrix calculation given projection values and a view matrix calculation given camera position.
cam :: (Num a, Epsilon a, Floating a) => (DView a) -> (GLState a) -> (GLState a)
cam view state = state { vMatrix= Just v, pMatrix= Just p}
								where 
								-- Converts degrees to radians.
								fovr = rad * fov view
								-- Constructs projecton matrix.
								p = perspective fovr (aspectRatio view) (znear view) (zfar view)
								-- Constructs view matrix. 
								v = lookAt (eye view) (tow view) (up view)

-- Degree to radian converter function.
rad :: (Floating a) => a
rad = pi/180

