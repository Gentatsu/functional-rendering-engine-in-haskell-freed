-- | Gets cube primitive from OBJ file.
module Graphics.FreeD.Rendering.Cube
	(getCube)
	where
import Graphics.UI.GLUT 
import Graphics.FreeD.Formats.ObjReader
import Data.Text hiding (concatMap)
import Graphics.FreeD.Data.Drawable
import Graphics.FreeD.Data.Misc
import Paths_FreeD
import Data.IORef 
import System.IO.Unsafe (unsafePerformIO)
import qualified Data.Map as Map

-- | Gets Cube given width, height, and depth.
getCube :: (Ord a, Num a, Fractional a) => a -> a -> a -> IO (Drawable a)
getCube w h d = do
	-- Gets the map reference for the cubes.
	let mapRef = getCubeRef
	-- Reads in the reference to get the map.
	map <- readIORef mapRef
	-- Looks up if the w, h, d have already been constructed.
	case Map.lookup (w,h,d) map of
		-- If they have, return them.
		Just comp -> return comp
		Nothing -> do
			-- Else, initialise the cube, and add it to the map.
			comp <- initCube w h d
			let map2 = Map.insert (w, h,d) comp map
			-- Replace old reference
			writeIORef mapRef map2
			return comp

-- | Parameter function which takes in the vertex and multiplies each x, y, z value with the height, width and depth respectively.
whd :: (Num a) => a -> a -> a -> (Drawable a) -> [(Vertex3 a, Normal3 a, TexCoord2 a)]
whd w h d (DObject (DGeometryVnt v) _) = concatMap (\(Vertex3 v1 v2 v3, n, t) -> [(Vertex3 (v1*w) (v2*h) (v3*d) , n, t)]) v

-- | Get map reference for cubes. Key is w, h, d, value is DObject.
getCubeRef :: IORef (Map.Map (a, a, a) (Drawable a))
{-# NOINLINE getCubeRef #-}
getCubeRef = unsafePerformIO $ newIORef Map.empty

-- | Initialise a cube, given width, height, and depth.
initCube :: (Num a, Fractional a) => a -> a -> a -> IO (Drawable a)
initCube w h d = do
	-- Reads cube OBJ in from data given in the paths when installed.
	file <- getDataFileName "data\\cube.obj"
	cubeContents <- readFile file
	-- Unpacks the loaded object, so that we can parameterise the values. 
	let obj = whd w h d $ loadObj $ pack cubeContents
	-- Packs the list in to a FreeD geometry type again.
	return $ DObject (DGeometryVnt obj) Triangles 
