-- | Gets sphere primitive from OBJ file.
module Graphics.FreeD.Rendering.Sphere
	(getSphere)
	where
import Graphics.UI.GLUT 
import Graphics.FreeD.Formats.ObjReader
import Data.Text hiding (concatMap)
import Graphics.FreeD.Data.Drawable
import Graphics.FreeD.Data.Misc
import Paths_FreeD
import Data.IORef
import System.IO.Unsafe (unsafePerformIO)
import qualified Data.Map as Map

-- | Gets a sphere, given radius.
getSphere :: (Ord a, Num a, Fractional a) => a -> IO (Drawable a)
getSphere r = do
	-- Gets the map reference for the spheres.
	let mapRef = getSphereRef
	-- Reads in the reference to get the map.
	map <- readIORef mapRef
	-- Looks up if a sphere with the radius has already been constructed.
	case Map.lookup r map of
		-- If it has, return it.
		Just comp -> return comp
		Nothing -> do
			-- Else, initialise the sphere, and add it to the map.
			comp <- initSphere r
			let map2 = Map.insert r comp map
			-- Replace old reference
			writeIORef mapRef map2
			return comp

-- | Parameter function which takes in the vertex and multiplies each x, y, z value with the radius.
rad :: (Num a) =>  a -> (Drawable a) -> [(Vertex3 a, Normal3 a, TexCoord2 a)]
rad r (DObject (DGeometryVnt v) _) = concatMap (\(Vertex3 v1 v2 v3, n, t) -> [(Vertex3 (v1*r) (v2*r) (v3*r) , n, t)]) v

-- | Get sphere map reference.
getSphereRef :: IORef (Map.Map a (Drawable a))
{-# NOINLINE getSphereRef #-}
getSphereRef = unsafePerformIO $ newIORef Map.empty	

-- | Initialise the sphere with given radius.
initSphere :: (Num a, Fractional a) => a -> IO (Drawable a)
initSphere r = do
	-- Reads sphere OBJ in from data given in the paths when installed.
	file <- getDataFileName "data\\sphere.obj"
	contents <- readFile file
	-- Unpacks the loaded object, so that we can parameterise the values. 
	let obj = rad r $ loadObj $ pack contents
	-- Packs the list in to a FreeD geometry type again.
	return $ DObject (DGeometryVnt obj) Triangles 
