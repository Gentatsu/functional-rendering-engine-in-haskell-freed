-- | Rendering functions for Drawable objects.
module Graphics.FreeD.Rendering.Drawable
	(
	  renderWithState
	, render
	, compile
	, compileWithState
	)
	
where
import Graphics.FreeD.Data.Drawable
import Graphics.FreeD.Data.Misc
import Graphics.FreeD.Rendering.Cube 
import Graphics.FreeD.Rendering.Sphere
import Graphics.FreeD.Shaders.DefaultShaders
import Graphics.FreeD.Shaders.ShaderSource
import Graphics.FreeD.Shaders.ShaderProgramType
import Graphics.FreeD.Rendering.GLState
import Graphics.FreeD.Data.GLState
import Graphics.FreeD.Data.Matrix
import Graphics.Rendering.OpenGL	               	(($=), get)
import qualified Graphics.Rendering.OpenGL.GL	        as GL
import qualified Graphics.Rendering.OpenGL.GLU.Errors   as GLU
import qualified Graphics.UI.GLUT		        as GLUT
import Graphics.GLUtil hiding (loadTexture, texHeight, texWidth, texData)
import Control.Monad (unless)
import Linear hiding (rotate)
import Data.Foldable (foldMap)
import qualified Data.Map as Map
import Data.IORef
import System.IO.Unsafe (unsafePerformIO)
import Foreign.Storable

-- | Renders a Drawable. If it is not compiled, it renders from the initial state, traversing down the scene graph.
render ::(GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a)), Ord a)  => Drawable a -> IO ()
render drawable = 
	-- Checks if it is a CompiledDrawable, Drawables, or any other.
	case drawable of 
		CompiledDrawable pMode' vboSize' divSize' vao' glState'  ->
			do
				-- If it is, we get the current shader program from the state.
				let p = getShaderProgram glState'
				GL.currentProgram $= Just (program p)
				-- Sets the modelview and projection matrix uniform variables.
				mvpUnif glState' p
				-- Checks if we have lighting.
				case lighting glState' of
					Just dlight -> do
						-- If we do, sets light uniform variables.
						lightUnif glState' p
						matUnif glState' p
					-- Else, set the colour uniform variable. Checks to make sure no textures set, first.
					Nothing -> case texture glState' of
						-- If we do, do nothing.
						Just _ -> return ()
						-- Else, set the colour uniform variable.
						Nothing -> colUnif glState' p
				-- Checks if we have a texture.
				case texture glState' of
					-- If we do, sets texture object uniform variable.
					Just tex -> texUnif glState' p
					-- Else, does nothing.
					Nothing -> return ()	
				-- Binds the VAO. 											
				GL.bindVertexArrayObject $= Just vao'
				-- Draws the mesh given primitive mode, size of array, and how many values per vertex.
				GL.drawArrays pMode' 0 $ fromIntegral vboSize' `div` fromIntegral divSize'
				-- Unbinds the VAO.
				GL.bindVertexArrayObject $= Nothing
		-- If we have a Blank, do nothing.
		Blank -> return ()
		-- If we have a Drawables, maps render function over them.
		Drawables drawables -> mapM_ render drawables
		-- Any other drawable, we compile and render. 
		_ -> do
			cd <- compile drawable
			render cd

-- | Render a Drawable given a state, we compile it with the state and then render.		
renderWithState ::(GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a)), Ord a)  => (GLState a) -> Drawable a -> IO ()
renderWithState state drawable = do
	cd <- compileWithState state drawable
	render cd


-- | Compiles given no state, initialises one and returns the CompiledDrawable.
compile ::(GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a)), Ord a)  => Drawable a -> IO (Drawable a) 
compile drawable = do
	-- Initialises the state.
	state <- initGLState   
	-- Compiles with the initialised state.
	compileWithState state drawable

-- | Compiles using a state, returns a CompiledDrawable or list of them. This is the main function that does all the work.
compileWithState :: (GL.UniformComponent a, Epsilon a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a)), Storable a, Ord a, Floating a, Fractional a)=> (GLState a) -> Drawable a -> IO (Drawable a)	  
compileWithState state drawable
 = {-# SCC "drawComponent" #-}
 	 -- Checks what Drawable we have.
   case drawable of

	-- Got nothin', returns nothin'.
	Blank
	 -> 	return Blank

	-- Geometry and mode to draw in. 
	DObject geom mode
		->	do
			{-comp <- compileGeometry geom mode state
			return comp {glState = state}-}
			let mapRef = getMapRef
			map <- readIORef mapRef
			case Map.lookup (geom, state) map of
				Just comp -> return $ comp {glState = state}
				Nothing -> do
					comp <- compileGeometry geom mode state
					let map2 = Map.insert (geom, state) comp map
					writeIORef mapRef map2
					return $ comp {glState=state}

	--  Sphere given a radius.
 	Sphere radius	
	 -> do
	 	-- Get the sphere (returns a DObject)
	 	;c <- getSphere radius
	 	-- Compiles down to one more level to return a CompiledDrawable.
	 	;compileWithState state c
	 
	-- Cube given width, height and depth.
	Cube width height depth 
	 ->	do
	  -- Get the cube (returns a DObject)
	 	;c <- getCube width height depth
	 	-- Compiles down to one more level to return a CompiledDrawable.
	 	;compileWithState state c 

	--  Render Drawable with given color.
	Color c d
		-> 
			-- Overwrites current material properties with new color, setting them all to be the color.
			-- Also overwrites current color set.
			let 
				mat =  Mat {matAmb = c, matDif = c, matSpec = c }
				newState = state {material = Just mat, color = c}
			-- Compiles drawable given new state.
			in compileWithState newState d
		
	--  Render Drawable with given Material properties (if lighting is used). 
	Material m d
	 ->  
	 	-- Overwrites current material properties.
	  let newState = state {material= Just m}
		in compileWithState newState d

	-- Transformations
	--  Constructs matrix, then multiplies with current one in state and overwrites current one for the new state.
	
	--  Scales the model matrix with factors x, y, and z.
	Scale x y z d
		->
			-- Performs scale functon on current model matrix.
			let newState = scale x y z state
			in compileWithState newState d

  --  Translates model matrix given x, y, z amounts.
	Translate x y z d
		->	
			let newState = translate x y z state
			in compileWithState newState d
			
	--  Rotates model matrix given angle (in degrees), and x, y, z axis.
	Rotate deg xAxis yAxis zAxis d
	 -> 
		let newState = rotate deg xAxis yAxis zAxis state
		in compileWithState newState d 

	--  Constructs view and projection matrices and overwrites current ones. 
	--  Renders from camera and perspective properties.
	Camera view  d
		->	 
		let newState = cam view state
		in compileWithState newState d

	--  Overwrites current lighting properties. Renders given these lighting properties.
	Lighting dlight d
		->	
		let newState = case material state of
			-- We check if the material has been set before, and if not, let it equal the current color (default is white ( 1 1 1 1))
			Nothing -> 
				let 
					col = color state
					mat = Just Mat {matAmb = col, matDif = col, matSpec = col }
				in state {lighting= Just dlight, material=mat} 
			-- If the material is already set, just overwrites lighting properties.
			(Just _) -> state {lighting =Just dlight}
		in compileWithState newState d
		
	--  Sets texture to given texture in state.
	Texture tex d
	 -> do	
		let newState = state {texture =Just tex}
		compileWithState newState d

	--  Turns texture off. Sets texture property to Nothing.
	TextureOff p
		-> do
		let newState = state {texture=Nothing}
		compileWithState newState p

	--  Maps compileWithState function to each Drawable in list, with current state. Each inherits it.
	Drawables ps
	 -> do
	 	cd <-  mapM (compileWithState state) ps
	 	return $ Drawables cd

	--  Returns itself and combines the current state with the one in the CompiledDrawable.
	CompiledDrawable  p vsize dsize v oldState
		->	do
		-- If we have new lighting, set it to that.
		let light = case lighting state of
			Just l ->	Just l
			Nothing -> lighting oldState
		-- If we have a new texture, set it to that.
		let tex = case texture state of
			Just t -> Just t
			Nothing -> texture oldState
		-- If we have a new material, set it to that.
		let mat = case material state of
			Just m -> Just m
			Nothing -> material oldState
		-- If we have a new projection matrix, set it to that.
		let pMat = case pMatrix state of
			Just p -> Just p
			Nothing -> pMatrix oldState
		-- If we have a new view matrix, set it to that.
		let vMat = case vMatrix state of
			Just v -> Just v
			Nothing -> vMatrix oldState 
		-- Multiply the old matrices with the new ones, and sets the other properties (might screw up view and projection matrix, be careful!)
		let newState = state {lighting=light, texture=tex, material=mat, mMatrix = mMatrix state !*! mMatrix oldState, pMatrix =  pMat, vMatrix= vMat} 
		-- Returns the CompiledDrawable with new state.
		return CompiledDrawable {pMode=p, vboSize=vsize, divSize=dsize, vao=v, glState=newState}

	--  Sets shader program to given shader program in state.
	Shader sp p
		->	do
			let newState = state {sProgram= Just sp}
			compileWithState newState p


-- | Errors ---------------------------------------------------------------------
-- Checks if any errors have been reported by OpenGL.
checkErrors :: String -> IO ()
checkErrors place
 = do   errors          <- get  GLU.errors
        unless (null errors)
         $ mapM_ (handleError place) errors

-- Handle the error by printing statement. Taken from Gloss.
handleError :: String -> GLU.Error -> IO ()
handleError place err
 = case err of
    GLU.Error GLU.StackOverflow _
     -> error $ unlines 
      [ "FreeD / OpenGL Stack Overflow " ++ show place
      , "  This program uses the FreeD vector graphics library, which tried to"
      , "  draw a Drawable using more nested transforms (Translate/Rotate/Scale)"
      , "  than your OpenGL implementation supports. The OpenGL spec requires"
      , "  all implementations to have a transform stack depth of at least 32,"
      , "  and FreeD tries not to push the stack when it doesn't have to, but"
      , "  that still wasn't enough."
      , ""
      , "  You should complain to your harware vendor that they don't provide"
      , "  a better way to handle this situation at the OpenGL API level."
      , ""
      , "  To make this program work you'll need to reduce the number of nested"
      , "  transforms used when defining the Drawable given to FreeD. Sorry." ]

    -- Issue #32: Spurious "Invalid Operation" errors under Windows 7 64-bit.
    --   When using GLUT under Windows 7 it complains about InvalidOperation, 
    --   but doesn't provide any other details. All the examples look ok, so 
    --   we're just ignoring the error for now.
    GLU.Error GLU.InvalidOperation _
     -> return ()
    _ 
     -> error $ unlines 
     [  "FreeD / OpenGL Internal Error " ++ show place
     ,  "  Please report this to navs92@hotmail.com."
     ,  show err ]

-- | Function to compile geometry into a CompiledDrawable
compileGeometry :: (GL.UniformComponent a, Epsilon a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a)), Storable a, Floating a)=> DGeometry a-> GL.PrimitiveMode -> GLState a -> IO (Drawable a)
compileGeometry geom mode state= do
	-- We flatten the geometry so it is a list of raw GLfloat values.
		let flat = flatten geom
		-- Calculate the size of the flattened list.
		let size = fromIntegral $ length flat
		-- Get how many values per vertex in list based on geometry type.
		let dSize = case geom of 
			DGeometryV _ ->	3
			DGeometryVnt _-> 8
			DGeometryVt _	->	5
			_ -> 6
		-- Stride is the the size of a GLfloat multiplied by the dSize. 
		let stride = varSize dSize
		-- Make a VBO from the flattened list.
		buff <- makeBuffer GL.ArrayBuffer flat
		-- Bind the VBO.
		GL.bindBuffer GL.ArrayBuffer $= Just buff
		-- Make a VAO.
		[vao'] <- GL.genObjectNames 1
		-- Bind the VAO.
		GL.bindVertexArrayObject $= Just vao'
		-- Initiliases the shader program based on the state, stride and dSize. This sets the attributes, uniform variables and builds, compiles and links the shaders.
		initShaderProgram state stride dSize
		-- Unbinds VAO and VBO.
		GL.bindVertexArrayObject $= Nothing
		GL.bindBuffer GL.ArrayBuffer $= Nothing
		-- Initialise another state to be returned, as the other one is not needed when returning.
		state2 <- initGLState
		-- Returns CompiledDrawable. 
		return CompiledDrawable {pMode=mode, vboSize=size, divSize=fromIntegral dSize, vao=vao', glState=state2}

-- | Gets the map IO reference for our compiled geometry nodes.
getMapRef :: IORef (Map.Map (DGeometry a, GLState a) (Drawable a))
{-# NOINLINE getMapRef #-}
getMapRef = unsafePerformIO $ newIORef Map.empty

-- | Flattens geometry down to a list of GLfloats.
flatten :: DGeometry a-> [a]
flatten (DGeometryV vs) = concatMap (\(GL.Vertex3 v1 v2 v3) -> [v1, v2, v3]) vs
flatten (DGeometryVn vns) = concatMap (\(GL.Vertex3 v1 v2 v3, GL.Normal3 n1 n2 n3) -> [v1, v2, v3, n1, n2, n3]) vns
flatten (DGeometryVt vts) = concatMap (\(GL.Vertex3 v1 v2 v3, GL.TexCoord2 t1 t2) -> [v1, v2, v3, t1, t2]) vts
flatten (DGeometryVnt vnts)= concatMap (\(GL.Vertex3 v1 v2 v3, GL.Normal3 n1 n2 n3, GL.TexCoord2 t1 t2) -> [v1, v2, v3, n1, n2, n3, t1, t2]) vnts
-- One normal for the list of vertices, maps the normal to every vertex and concatonates the result.
flatten (DGeometryNv (GL.Normal3 n1 n2 n3) vs)= concatMap (\(GL.Vertex3 v1 v2 v3) -> [v1, v2, v3, n1, n2, n3]) vs
-- One normal for every three vertices, replicates the normal for each vertex in the flattened list. 
flatten (DGeometryNt nts) = concatMap (\(GL.Normal3 n1 n2 n3, (GL.Vertex3 v1 v2 v3, GL.Vertex3 v4 v5 v6, GL.Vertex3 v7 v8 v9)) -> [v1, v2, v3, n1, n2, n3, v4,v5,v6,n1,n2,n3,v7,v8,v9, n1, n2, n3]) nts


