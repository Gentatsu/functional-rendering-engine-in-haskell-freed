-- | ShaderProgramType data type and initialiser.
module Graphics.FreeD.Shaders.ShaderProgramType
	(
		ShaderProgramType(..),
		initSPT
	)
	
where
import Graphics.FreeD.Shaders.ShaderSource
import Graphics.GLUtil (ShaderProgram(..), simpleShaderProgramBS)
import Data.Monoid

-- | Record syntax for ShaderProgramType.
data ShaderProgramType = ShaderProgramType 
	{
		 sSP :: ShaderProgram  -- ^ Simple shader
		,tSP :: ShaderProgram  -- ^ Texture shader
		,pSP :: ShaderProgram  -- ^ Phong shader
		,ptSP :: ShaderProgram -- ^ Phong + Texture shader
		,fSP :: ShaderProgram  -- ^ Flat shader
		,ftSP :: ShaderProgram -- ^ Flat + Texture shader
		,gSP :: ShaderProgram  -- ^ Gourad shader
		,gtSP :: ShaderProgram -- ^ Gourad + Texture shader 
	}	
	deriving (Eq, Show, Ord)

-- Instances

-- | Show instance for the sake of being part of other data types deriving show.
instance Show ShaderProgram where
	show sp = show $ program sp
-- | Eq instance, uses the program of the shader program to compare.
instance Eq ShaderProgram where
  sp1 == sp2 = program sp1 == program sp2
-- | Ord instance, uses the program of the shader program to be ordered. 
instance Ord ShaderProgram where
  sp1 `compare` sp2 =  program sp1 `compare` program sp2

-- | Initialises ShaderProgramType with each shader program type. 
initSPT = do
	sSP' <- simpleShaderProgramBS sVert sFrag
	tSP' <- simpleShaderProgramBS tVert tFrag
	pSP' <- simpleShaderProgramBS pVert pFrag
	ptSP' <- simpleShaderProgramBS ptVert ptFrag
	fSP' <- simpleShaderProgramBS fVert fFrag
	ftSP' <- simpleShaderProgramBS ftVert ftFrag
	gSP' <- simpleShaderProgramBS gVert gFrag
	gtSP' <- simpleShaderProgramBS gtVert gtFrag
	return ShaderProgramType {sSP=sSP', tSP=tSP', pSP=pSP', ptSP=ptSP', fSP=fSP', ftSP=ftSP', gSP=gSP',gtSP=gtSP'}
