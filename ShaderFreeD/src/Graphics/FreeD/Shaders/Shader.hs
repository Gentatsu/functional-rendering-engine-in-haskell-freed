{-# LANGUAGE OverloadedStrings #-}

{- | TAKEN FROM https://github.com/bananu7/Hate/tree/5207395c5e02515323178b8b11f7430df483bb39
	   Documented by Naval Bhandari (Author of FreeD)
	   Module for representing shader boiler plate code as strings. Doesn't include main function or separate functions support.
-}
module Graphics.FreeD.Shaders.Shader where
import qualified Data.ByteString.Char8 as BS (ByteString, pack)

-- | String representation of Maybe type
showMaybe :: Show a => Maybe a -> String
showMaybe (Just x) = show x
showMaybe Nothing = ""

type Name = String

-- | The different types of versions a shader can have.
data Version = Version150 | Version330 | Version440 | Version450
instance Show Version where
		show Version150 = "#version 150 core"
		show Version330 = "#version 330 core"
		show Version440 = "#version 440 core"
		show Version450 = "#version 450 core"

-- | The precision used in the shader for floating point numbers.
data FloatPrecision = HighPrecision | MediumPrecision | LowPrecision
instance Show FloatPrecision where
    show HighPrecision      = "precision highp float;"
    show MediumPrecision    = "precision mediump float;"
    show LowPrecision       = "precision lowp float;"

-- | The location of a variable (not used).
newtype Location = Location Int
instance Show Location where 
    show (Location loc) =  "layout(location = " ++ show loc ++ ") "

-- | Data type of a variable.
data TypeTag = FloatTag | Vec2Tag | Vec3Tag | Vec4Tag | Mat2Tag | Mat3Tag | Mat4Tag | Sampler2DTag 
instance Show TypeTag where
    show FloatTag = "float"
    show Vec2Tag = "vec2"
    show Vec3Tag = "vec3"
    show Vec4Tag = "vec4"
    show Mat2Tag = "mat2"
    show Mat3Tag = "mat3"
    show Mat4Tag = "mat4"
    show Sampler2DTag = "sampler2D"

-- | Data type for in variable declaration. I've added FlatInput as a constructor, so that variables can be flat. Possibly a bug,  in that it can loop forever.
data Input = Input TypeTag (Maybe Location) Name | FlatInput Input
instance Show Input where
    show (Input tag loc name) = showMaybe loc ++ "in " ++ show tag ++ " " ++ name ++ ";"
    show (FlatInput i) = "flat " ++ show i

-- | Data type for out variable declaration. I've added FlatOutput as a constructer, so that variables can be flat. Possibly a buf, in that it can loop forever.
data Output = Output TypeTag Name | FlatOutput Output
instance Show Output where
    show (Output tag name) = "out " ++ show tag ++ " " ++ name ++ ";"
    show (FlatOutput i) = "flat " ++ show i

-- Binding variable (not used).
newtype Binding = Binding Int
instance Show Binding where 
    show (Binding bnd) =  "layout(binding = " ++ show bnd ++ ") "

-- Data type for uniform variable declaration. 
data Uniform = Uniform TypeTag (Maybe Binding) Name
instance Show Uniform where
    show (Uniform tag bnd name) = showMaybe bnd ++ "uniform " ++ show tag ++ " " ++ name ++ ";"

-- |This is used simply for the purposes of full pipeline construction
data Varying = Varying TypeTag Name

-- | To convert a Varying to an in variable.
toInput :: Varying -> Input
toInput (Varying tag name) = Input tag Nothing name

-- | To convert a Varying to an out variable.
toOutput :: Varying -> Output
toOutput (Varying tag name) = Output tag name

-- | Function to produce a string representation of a shader. 
shaderStr :: 
    Version ->
    FloatPrecision ->
    [Input] ->
    [Output] ->
    [Uniform] ->
    String ->
    String
shaderStr v p ins outs unifs body = header ++ "void main() {\n" ++ body ++ "\n}\n"
    where versionStr = show v
          precisionStr = show p
          inputsStr = unlines . map show $ ins
          outputsStr = unlines . map show $ outs
          uniformsStr = unlines . map show $ unifs
          header = unlines [versionStr, precisionStr, inputsStr, outputsStr, uniformsStr]
          
-- | Function to produce a bytestring representation of a shader. 
shader :: 
    Version ->
    FloatPrecision ->
    [Input] ->
    [Output] ->
    [Uniform] ->
    String ->
    BS.ByteString
shader v p i o u b = BS.pack $ shaderStr v p i o u b
