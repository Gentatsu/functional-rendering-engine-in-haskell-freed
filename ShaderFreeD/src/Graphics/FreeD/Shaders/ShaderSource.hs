-- | Functions for building vertex and fragment shaders.
module Graphics.FreeD.Shaders.ShaderSource
	(
		  sVert
		 ,sFrag
		 ,pVert
		 ,pFrag
		 ,gVert
		 ,gFrag
		 ,fVert
		 ,fFrag
		 ,tVert
		 ,tFrag
		 ,ptVert
		 ,ptFrag
		 ,gtVert
		 ,gtFrag
		 ,ftVert
		 ,ftFrag
		 ,vPositionVar
		 ,vNormalVar
		 ,vTexCoordVar
		 ,uModelViewMatrixVar
		 ,uProjectionMatrixVar
		 ,uNormalMatrixVar
		 ,uLightPosVar
		 ,uLightAmbVar
		 ,uLightDifVar
		 ,uLightSpecVar
		 ,uTexUnitVar
		 ,uMatAmbVar
		 ,uMatSpecVar
		 ,uMatDifVar
		 ,uColorVar
	)
	
where
import Graphics.FreeD.Shaders.Shader
import qualified Data.ByteString.Char8 as BS (ByteString, pack)

-- Default version.
vers = Version330
-- Default precision.
p = HighPrecision
-- Shader endline with newline.
endl = ";\n"
-- To let a variable equal another.
v1 `toEqual` v2 = v1 ++ " = " ++ v2 ++ endl

--Variables Names
------------------------------------
eyeCordFsVar = "eyeCordFs"
eyeNormalFsVar = "eyeNormalFs"
	--Vertex
vPositionVar = "vPosition"
vNormalVar = "vNormal"
vTexCoordVar = "vTexCoord"
vColorVar = "vColor"
	--Fragment
fColorVar = "fColor"
fTexCoordVar = "fTexCoord"
	--Uniform
uModelViewMatrixVar = "uModelViewMatrix"
uProjectionMatrixVar = "uProjectionMatrix"
uNormalMatrixVar = "uNormalMatrix"
uLightPosVar = "uLightPos"
uLightAmbVar = "uLightAmb"
uLightDifVar = "uLightDif"
uLightSpecVar = "uLightSpec"
uColorVar = "uColor"
uTexUnitVar = "uTexUnit"
uMatAmbVar = "uMatAmb"
uMatDifVar = "uMatDif"
uMatSpecVar = "uMatSpec"

--Inputs 
-------------------------------------------------------------
	--Vertex
vPosition = Input Vec3Tag Nothing vPositionVar
vNormal = Input Vec3Tag Nothing vNormalVar
vTexCoord = Input Vec2Tag Nothing vTexCoordVar

	--Fragment
inEyeCoordFs = Input Vec4Tag Nothing eyeCordFsVar
inEyeNormalFs = Input Vec4Tag Nothing eyeNormalFsVar
inFTexCoord = Input Vec2Tag Nothing fTexCoordVar

--Outputs
-------------------------------------------------------------
	--Vertex
outEyeCordFs = Output Vec4Tag eyeCordFsVar
outEyeNormalFs = Output Vec4Tag eyeNormalFsVar
outFTexCoord = Output Vec2Tag fTexCoordVar
vColor = Varying Vec4Tag vColorVar

	--Fragment
fColor = Output Vec4Tag fColorVar

-- Default out variable for fragment shader.
outFColor = [fColor]


--Uniform 
----------------------------------------------------------------------
	-- In all
uModelViewMatrix = Uniform Mat4Tag Nothing uModelViewMatrixVar
uProjectionMatrix = Uniform Mat4Tag Nothing uProjectionMatrixVar
	-- Light
uNormalMatrix = Uniform Mat3Tag Nothing uNormalMatrixVar
uLightPos = Uniform Vec4Tag Nothing uLightPosVar
uLightAmb = Uniform Vec4Tag Nothing uLightAmbVar
uLightDif = Uniform Vec4Tag Nothing uLightDifVar
uLightSpec = Uniform Vec4Tag Nothing uLightSpecVar
uMatAmb = Uniform Vec4Tag Nothing uMatAmbVar
uMatDif = Uniform Vec4Tag Nothing uMatDifVar
uMatSpec = Uniform Vec4Tag Nothing uMatSpecVar
	-- Texture
uTexUnit = Uniform Sampler2DTag Nothing uTexUnitVar
	-- Misc
uColor = Uniform Vec4Tag Nothing uColorVar

--Body
--------------------------------------------------------------------------------------------------------------------------------------------
--All
-- Calculate the position of a vertex based off of projection and modelview matrix. 
glPosition = "gl_Position = " ++ uProjectionMatrixVar ++ " * " ++ uModelViewMatrixVar ++ " * vec4(" ++ vPositionVar ++ ", 1.0)"  ++ endl
eyeNormalVar = "eyeNormal"
eyeCordVar = "eyeCord"
-- Calculate the normal from the current modelview matrix using the normal matrix.
eyeNormal = "vec4 " ++ eyeNormalVar ++ " = vec4(normalize(" ++ uNormalMatrixVar ++ "* " ++ vNormalVar ++"), 0.0)" ++ endl
eyeCord = "vec4 " ++ eyeCordVar ++ " = " ++ uModelViewMatrixVar ++ " * vec4(" ++ vPositionVar ++ ", 1.0)" ++ endl
eyeNormalCord = eyeNormal ++ eyeCord ++ eyeCordFsVar `toEqual` eyeCordVar ++ eyeNormalFsVar `toEqual` eyeNormalVar
initEye = "vec4 " ++ eyeCordFsVar  ++ endl ++ "vec4 " ++ eyeNormalFsVar ++ endl

--Texture
--------------------------------------------------------------------------------------------------------------------------------------------
vToFTexCoord = fTexCoordVar `toEqual` vTexCoordVar
texColorVar = "texColor"
-- Retrieves texture colour from coordinates and currently binded texture.
texColor  = "vec4 " ++ texColorVar ++ " = texture(" ++ uTexUnitVar ++ ", " ++ fTexCoordVar ++ ")" ++ endl

--Light 
--------------------------------------------------------------------------------------------------------------------------------------------
-- Phong Lighting Model calculations
sVar = "s"
s = "vec4 " ++ sVar ++ " = normalize(" ++ uLightPosVar ++ " - " ++ eyeCordFsVar ++" ) " ++ endl
--rVar = "r"
--r = "vec4 " ++ rVar ++ " = normalize(reflect(-" ++ sVar ++ ","++eyeNormalFsVar ++ "))" ++ endl
hVar = "h"
h = "vec4 " ++ hVar ++ " = (" ++ sVar ++ " + " ++ vVar ++ ") / 2 " ++ endl
vVar = "v"
v = "vec4 " ++ vVar ++ " = normalize(-" ++ eyeCordFsVar ++" )" ++ endl
specVar = "spec"
spec = "float " ++ specVar ++ " = max( dot(" ++ eyeNormalFsVar ++ "," ++ hVar ++ "),0.0 )" ++ endl
diffVar = "diff"
diff = "float " ++ diffVar ++ " = max(dot(" ++ eyeNormalFsVar ++ "," ++ sVar ++ "),0.0)" ++ endl
lightEq = s ++ v ++ h ++ spec ++ diff
phongLM =  "(" ++ diffColorVar ++ " + " ++ specColorVar ++ " + " ++ ambientColorVar ++ ")"
lightToFColorInit = fColorVar ++ " = "  ++ phongLM
lightToVColor = vColorVar `toEqual` phongLM
lightToFColor = lightToFColorInit ++ endl
lightToFColorTex = lightToFColorInit ++ " * " ++ texColorVar ++ endl

diffColorVar = "diffColor" 
diffColorInit = "vec4 " ++ diffColorVar ++ " = " ++ "clamp(" ++ diffVar ++ " * " ++ uLightDifVar
ambientColorVar = "ambientColor"
ambientColorInit = "vec4 " ++ ambientColorVar ++ " = " ++ "clamp(" ++ uLightAmbVar
specColorVar = "specColor"
specColorInit =  "vec4 " ++ specColorVar ++ "  = clamp(pow(" ++ specVar ++ ",0.3) * " ++ uLightSpecVar

clamp01 = ", 0.0, 1.0)"

-- Material calculations
ambientColor = ambientColorInit ++ " * " ++ uMatAmbVar ++ clamp01 ++ endl
diffColor = diffColorInit ++ " * " ++ uMatDifVar ++ clamp01 ++ endl
specColor = specColorInit ++ " * " ++ uMatSpecVar ++ clamp01 ++ endl


-- SHADER CONSTRUCTION
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------

-- | Simple vertex shader.
-- Takens in vertex position, multiplies by modelview and projection matrices.
sVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition]
				outs = []
				unifs = [uModelViewMatrix, uProjectionMatrix]
				body = glPosition

-- | Simple fragment shader.
-- Sets the pixel colour to the one given. 
sFrag = shader vers p ins outFColor unifs body 
	where
				ins = []
				unifs = [uColor] 
				body = fColorVar `toEqual` uColorVar

-- | Texture vertex shader.
-- Same as sVert, but takes in a texture coordinate and outputs it to the fragment shader.
tVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vTexCoord]
				outs = [outFTexCoord]
				unifs = [uModelViewMatrix, uProjectionMatrix]
				body = vToFTexCoord ++ glPosition

-- | Texture fragment shader.
-- Retrieves the texture colou using the interpolated texture coordinates given a texture and sets the pixel to that colour.
tFrag = shader vers p ins outFColor unifs body 
	where
				ins = [inFTexCoord]
				unifs = [uTexUnit] 
				body = texColor ++ (fColorVar `toEqual` texColorVar)

-- | Phong vertex shader.
-- Same as sVert, but takes in a normal, and normal matrix. Calculates eye coordinate and normal and outputs to fragment shader.
pVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vNormal]
				outs = [outEyeCordFs, outEyeNormalFs]
				unifs = [uModelViewMatrix, uProjectionMatrix, uNormalMatrix]
				body = eyeNormalCord ++ glPosition

-- | Phong fragment shader.
-- Takes in eye coordinate and normal from pVert, and lighting and material properties, and does phong lighting model calculation to work out the light intensity and then sets the pixel colour to it.
pFrag = shader vers p ins outFColor unifs body 
	where
				ins = [inEyeCoordFs, inEyeNormalFs]
				unifs = [uLightPos, uLightAmb, uLightDif, uLightSpec, uMatAmb, uMatDif, uMatSpec] 
				body = lightEq ++ diffColor ++ ambientColor ++ specColor ++ lightToFColor

-- | Phong + Texture vertex shader.
-- Same as pVert and tVert together.
ptVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vNormal, vTexCoord]
				outs = [outEyeCordFs, outEyeNormalFs, outFTexCoord]
				unifs = [uModelViewMatrix, uProjectionMatrix, uNormalMatrix]
				body = eyeNormalCord ++ vToFTexCoord ++ glPosition


-- | Phong + Texture fragment shader.
-- Same as pFrag and tVert. Multiplies the light intensity and texture colour together.
ptFrag = shader vers p ins outFColor unifs body 
	where
				ins = [inEyeCoordFs, inEyeNormalFs, inFTexCoord]
				unifs = [uLightPos, uLightAmb, uLightDif, uLightSpec, uTexUnit,  uMatAmb, uMatDif, uMatSpec] 
				body = lightEq ++ texColor ++ diffColor ++ ambientColor ++ specColor ++ lightToFColorTex


-- | Gourad vertex shader.
-- Same as pVert and pFrag, and outputs light intensity to fragment shader.
gVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vNormal]
				outs = [toOutput vColor]
				unifs = [uModelViewMatrix, uProjectionMatrix, uNormalMatrix, uLightPos, uLightAmb, uLightDif, uLightSpec, uMatAmb, uMatDif, uMatSpec]
				body = initEye ++ eyeNormalCord ++ lightEq ++ diffColor ++ ambientColor ++ specColor ++ lightToVColor ++  glPosition

-- | Gourad fragment shader.
-- Takes in interpolated light intensity and sets the pixel colour to it. 
gFrag = shader vers p ins outFColor unifs body 
	where
				ins = [toInput vColor]
				unifs = [] 
				body = fColorVar `toEqual` vColorVar

-- | Gourad + Texture vertex shader.
-- Same as gVert and tVert.
gtVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vNormal, vTexCoord]
				outs = [toOutput vColor, outFTexCoord]
				unifs = [uModelViewMatrix, uProjectionMatrix, uNormalMatrix, uLightPos, uLightAmb, uLightDif, uLightSpec,  uMatAmb, uMatDif, uMatSpec]
				body = initEye ++ eyeNormalCord ++ lightEq ++ vToFTexCoord ++ diffColor ++ ambientColor ++ specColor ++ lightToVColor ++  glPosition

-- | Gourad + Texture fragment shader.
-- Same as gFrag and tFrag, multiplies the input colour with texture coordinate.
gtFrag = shader vers p ins outFColor unifs body 
	where
				ins = [toInput vColor, inFTexCoord]
				unifs = [uTexUnit] 
				body = texColor ++ fColorVar `toEqual` (vColorVar ++ " * " ++ texColorVar) 
				
-- | Flat vertex shader.
-- Same as gVert, except doesn't interpolate light intensity.
fVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vNormal]
				outs = [FlatOutput (toOutput vColor)]
				unifs = [uModelViewMatrix, uProjectionMatrix, uNormalMatrix, uLightPos, uLightAmb, uLightDif, uLightSpec, uMatAmb, uMatDif, uMatSpec]
				body = initEye ++ eyeNormalCord ++ lightEq ++ diffColor ++ ambientColor ++ specColor ++ lightToVColor ++  glPosition

-- | Flat fragment shader.
-- Same as gFrag, but doesn't interpolate color.
fFrag = shader vers p ins outFColor unifs body 
	where
				ins = [FlatInput (toInput vColor)]
				unifs = [] 
				body = fColorVar `toEqual` vColorVar 

-- | Flat + Texture vertex shader.
-- Same as fVert and tVert.
ftVert = shader vers p ins outs unifs body 
	where
				ins = [vPosition, vNormal, vTexCoord]
				outs = [FlatOutput (toOutput vColor), outFTexCoord]
				unifs = [uModelViewMatrix, uProjectionMatrix, uNormalMatrix, uLightPos, uLightAmb, uLightDif, uLightSpec,  uMatAmb, uMatDif, uMatSpec]
				body = initEye ++ eyeNormalCord ++ lightEq ++ vToFTexCoord ++ diffColor ++ ambientColor ++ specColor ++ lightToVColor ++  glPosition

-- | Flat + Texture fragment shader.
-- Same as fFrag and tFrag.
ftFrag = shader vers p ins outFColor unifs body 
	where
				ins = [FlatInput (toInput vColor), inFTexCoord]
				unifs = [uTexUnit] 
				body = texColor ++ fColorVar `toEqual` (vColorVar ++ " * " ++ texColorVar)




