{-# LANGUAGE FlexibleContexts #-}
-- | Default shader functions
module Graphics.FreeD.Shaders.DefaultShaders
	(
		 sShader
	  ,pShader
	  ,tShader
	  ,ptShader
	  ,gtShader
	  ,gShader
	  ,ftShader
	  ,fShader
	  ,varSize
	  ,getShaderProgram
	  ,initShaderProgram
	  ,mvpUnif
	  ,lightUnif
	  ,texUnif
	  ,matUnif
	  ,colUnif
	)
	
where
import Graphics.FreeD.Data.Drawable
import Graphics.FreeD.Data.Matrix
import Graphics.FreeD.Data.GLState
import Graphics.FreeD.Data.Misc
import Graphics.FreeD.Shaders.ShaderProgramType
import Graphics.FreeD.Shaders.ShaderSource
import Graphics.Rendering.OpenGL	               	(($=), get)
import qualified Graphics.Rendering.OpenGL       as GL
import Graphics.Rendering.OpenGL.GL.Shaders.ShaderObjects
import Linear 
import Foreign.Storable (sizeOf)
import Graphics.GLUtil hiding (loadTexture, texHeight, texWidth, texData)
import Data.Map ((!))
import qualified Data.ByteString as B (putStrLn)
import Control.Monad(unless)

-- | Calculate the size of an amount of GLfloats.
varSize n = fromIntegral $ sizeOf (undefined::GL.GLfloat) * n

-- | Initialise the shader program.
shaderInit :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> ShaderProgram -> IO ()
shaderInit state stride p = do
	-- Only change the current shader program if it is not set.
	cP <- get GL.currentProgram
	;unless (cP == Just (program p)) $
		GL.currentProgram $= Just (program p)
	-- Set the vertex position, which is available in EVERY vertex shader.
	;let posn = fst $ attribs p ! vPositionVar
	;let posVad = GL.VertexArrayDescriptor 3 GL.Float stride offset0
	;GL.vertexAttribPointer posn $= (GL.ToFloat, posVad)
	;GL.vertexAttribArray posn   $= GL.Enabled
	-- Set the mvp matrices, which is available in EVERY vertex shader.
	;mvpUnif state p

-- | Initialise the light attributes.
lightInit :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> ShaderProgram -> IO ()
lightInit state stride p = do
	-- Set normal.
	;let normal = fst $ attribs p ! vNormalVar
	;let norVad = GL.VertexArrayDescriptor 3 GL.Float stride (offsetPtr (varSize 3))
	;GL.vertexAttribPointer normal $= (GL.ToFloat, norVad)
	;GL.vertexAttribArray normal  $= GL.Enabled
	-- Set light properties
	;lightUnif state p

-- | Initialise texture attributes.
texInit :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> Int -> ShaderProgram -> IO ()
texInit state dSize p = do
	-- Set texture coordinates.
	;let texCoord = fst $ attribs p ! vTexCoordVar
	;let stride = varSize dSize
	;let texVad = GL.VertexArrayDescriptor 2 GL.Float stride (offsetPtr (varSize (dSize-2)))
	;GL.vertexAttribPointer texCoord $= (GL.ToFloat, texVad)
	;GL.vertexAttribArray texCoord  $= GL.Enabled
	-- Set texture object.
	;texUnif state p

-- | Combination of shader initial shader and light initialisations.
shaderAndLight :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> ShaderProgram -> IO ()
shaderAndLight state stride p= do	
	shaderInit state stride p
	lightInit state stride p

-- | Combination of light and material initialisations.
lightAndMat :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> ShaderProgram -> IO ()
lightAndMat state stride p = do
	;shaderAndLight state stride p 
	;matUnif state p

-- | Combination of light and texture initialisations.
lightAndTex :: (GL.UniformComponent a, Num a, Epsilon a, Floating a,  AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> ShaderProgram -> Int -> IO ()
lightAndTex state stride p dSize = do
  ;lightAndMat state stride p  
	;texInit state dSize p
	
-- | Simple Shader
sShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> IO ()
sShader state stride = do
	let p = sSP (shaderProgramType state)
	;shaderInit state stride p
	;colUnif state p

-- | Texture Shader
tShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> Int -> IO ()
tShader state stride dSize = do
	let p = tSP (shaderProgramType state)
	;shaderInit state stride p
	;texInit state dSize p
	
-- | Phong Shading + Texture Shader
ptShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> Int -> IO ()
ptShader state stride dSize = do
	let p = ptSP (shaderProgramType state)
	lightAndTex state stride p dSize

-- | Gourad Shading + Texture Shader
gtShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> Int -> IO ()
gtShader state stride dSize = do
	let p = gtSP (shaderProgramType state)
	lightAndTex state stride p dSize

-- | Flat Shading + Texture Shader
ftShader ::(GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> Int -> IO ()
ftShader state stride dSize = do
	let p = ftSP (shaderProgramType state)
	lightAndTex state stride p dSize

-- | Phong Shader
pShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> IO ()
pShader state stride = do
	let p = pSP (shaderProgramType state)
	lightAndMat state stride p

-- | Gourad Shader
gShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> IO ()
gShader state stride = do
	let p = gSP (shaderProgramType state)
	lightAndMat state stride p

-- | Flat Shader
fShader :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> IO ()
fShader state stride = do
	let p = fSP (shaderProgramType state)
	lightAndMat state stride p

-- | Get shader program functions initialiser based on lighting and shading model. 
initLightProg state = case lighting state of
						Just light -> case shadingModel light of
							Flat -> (ftShader, fShader)
							Gourad -> (gtShader, gShader)
							Phong  -> (ptShader, pShader)
						Nothing -> (tShader, sShader)


-- | Initialise shader program based on state. 
initShaderProgram :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> GL.Stride -> Int -> IO ()
initShaderProgram state stride dSize = 
	-- If a custom shader is used.
	case sProgram state of
		(Just sp) -> GL.currentProgram $=  Just (program sp)
		Nothing -> do
				-- Get texture and non-texture functions.
				let (tp, notp) = initLightProg state
				-- If texture is set, use shader program with it, else not.
				case texture state of 
					Just _  -> tp state stride dSize
					Nothing -> notp state stride 

-- | Gets the current shader program function for use with the cache ShaderProgramType.
getLightProg state = case lighting state of
						Just light -> case shadingModel light of
							Flat -> (ftSP, fSP)
							Gourad -> (gtSP, gSP)
							Phong  -> (ptSP,pSP)
						Nothing -> (tSP, sSP)

-- | gets the current cached shader program.
getShaderProgram :: (GLState a) -> ShaderProgram
getShaderProgram state = 
	-- Same as init check.
	case sProgram state of
		(Just sp) -> sp
		Nothing ->	 do
				let (tp, notp) = getLightProg state
				case texture state of
					Just _ -> tp spt
					Nothing -> notp spt
	where
		spt = shaderProgramType state

-- | Sets the modelview and projection matrix uniform variables.
mvpUnif :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> ShaderProgram  -> IO ()
mvpUnif state p = do
	-- Check if view and projection matrices are there, else set them to the identity.
	let vMat = case vMatrix state of
		Just v -> v
		Nothing -> getIdentity
	let pMat = case pMatrix state of
		Just p -> p
		Nothing -> getIdentity
  -- Multiply model and view matrix together.
	let mvMatrix = vMat !*! mMatrix state
	--let modelViewMatrixUniform = getUniform p uModelViewMatrixVar
	--let projectionMatrixUniform = getUniform p uProjectionMatrixVar
	setUniform p uModelViewMatrixVar mvMatrix
	setUniform p uProjectionMatrixVar pMat
	--setUniformMatrix modelViewMatrixUniform mvMatrix
	--setUniformMatrix projectionMatrixUniform pMat


-- |Sets light uniform variables.
lightUnif :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V3 (V3 a))) => (GLState a) -> ShaderProgram -> IO ()
lightUnif state p = do
	-- Retrieves light from state
	;let dlight = fromMaybe (lighting state)
	-- Checks if view matrix there, else sets it to identity.
	let vMat = case vMatrix state of
		Just v -> v
		Nothing -> getIdentity
	-- Get modelview matrix.
	;let mvMatrix = vMat !*! mMatrix state
	-- Converts modelview matrix from 4x4 matrix to 3x3 matrix.
	;let mv3 = m44_to_m33 mvMatrix
	-- Inverse of 3x3 modelview matrix.
	;let inv = inv33 mv3 
	-- Checks if we get a valid inverse matrix.
	case inv of 
		Just n -> 	do
		-- Transpose the inverse.
			let nMatrix = transpose n
			;setUniform p uNormalMatrixVar nMatrix
			;setUniform p uLightPosVar $ pos dlight
			;setUniform p uLightAmbVar $ amb dlight
			;setUniform p uLightDifVar $ dif dlight
			;setUniform p uLightSpecVar $ spec dlight
		-- If no valid inverse is returned, we do not set the uniform variables.
		Nothing -> return ()

-- | Set material uniform variables.
matUnif :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> ShaderProgram -> IO()
matUnif state p = 
	-- Sets only if we have a Just value.
	case material state of
		Just mat ->	do
			;setUniform p uMatAmbVar $ matAmb mat
			;setUniform p uMatDifVar $ matDif mat
			;setUniform p uMatSpecVar $ matSpec mat
		Nothing -> return ()

-- | Set texture object uniform variable.
texUnif :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> ShaderProgram -> IO ()
texUnif state p = do
	-- Binds current texture in state.
	;GL.textureBinding GL.Texture2D $= texture state
	-- Sets active texture unit to 0 (Only one in shader).
	;GL.activeTexture $= GL.TextureUnit 0
	;setUniform p uTexUnitVar (GL.TextureUnit 0)

-- | Set the color uniform variable.
colUnif :: (GL.UniformComponent a, Num a, Epsilon a, Floating a, AsUniform (V4 (V4 a)), AsUniform (V3 (V3 a))) => (GLState a) -> ShaderProgram -> IO ()
colUnif state p = setUniform p uColorVar $ color state

-- Gets value from a Maybe constructor.
fromMaybe (Just x) =  x
