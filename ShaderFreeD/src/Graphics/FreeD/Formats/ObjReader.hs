-- | Obj Reader parser to FreeD's geometry data types.
module Graphics.FreeD.Formats.ObjReader
	(
	  loadObj
	)
where
import Graphics.FreeD.Data.Drawable
import Graphics.FreeD.Data.Misc
import Graphics.Rendering.OpenGL.GL (PrimitiveMode(..), Normal3(..), TexCoord2(..), Vertex3(..)) 
import Data.Text (Text)
import Graphics.OBJ.Parser 

-- | Loads an obj file's contents in to a Drawable of type DObject DGeometryVnt Triangles
loadObj :: (Fractional a) => Text -> (Drawable a)
loadObj contents = 
						-- Tries to parse OBJ file.
						let obj = tryParseOBJFile contents
						in case obj of
							-- If it fails, return a Blank object.
							Left err ->  Blank
							-- If it suceeds, return the geometry. 
							Right x -> DObject (DGeometryVnt $ concatMap getFace $ faces x) Triangles 

-- Converts a Face in to a list of triples.
getFace :: (Fractional a) => Face -> [(Vertex3 a, Normal3 a, TexCoord2 a)] 
getFace (Face (v1, vn1, t1) (v2, vn2, t2) (v3, vn3, t3)) = [(vTov3 v1, nTon3 vn1, tTot2 t1), (vTov3 v2, nTon3 vn2, tTot2 t2), (vTov3 v3, nTon3 vn3, tTot2 t3)] 

-- Conversion functions from obj-parser library to FreeD's types.
-- Simply deconstructs and reconstructs.

vTov3 :: (Fractional t) => Vertex -> Vertex3 t
vTov3 (Vertex a b c) = Vertex3 (realToFrac a) (realToFrac b) (realToFrac c)

nTon3 :: (Fractional t) => VertexNormal -> Normal3 t
nTon3 (VertexNormal a b c) = Normal3 (realToFrac a) (realToFrac b) (realToFrac c)

tTot2 :: (Fractional t) => TextureCoord -> TexCoord2 t
tTot2 (TextureCoord a b) = TexCoord2 (realToFrac a) (realToFrac b)


