-----------------------------------------------------------------------------
--
-- Module      :  FreeD
--
-- Maintainer  :  Naval Bhandari
-- Stability   :  Experimental
-- Portability :  Portable
--
{- |
 FreeD provides a scene graph approach to rendering graphics using OpenGL 2.1+. It is built on top of the programmable pipeline (i.e. non-fixed function) of
 OpenGL 2.1 and uses features such as vertex buffer objects (VBO's), texture objects and GLSL shader code synthetisation to create graphics programs. 
 
	Below we show a MWE of using FreeD. 

	@
	import Graphics.UI.GLUT
	import Graphics.FreeD
	
	main :: IO ()
	main = do
		(_progName, _args) <- getArgsAndInitialize
		initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
		_window <- createWindow ""
		initialWindowSize $= Size 400 400
		reshapeCallback $= Just reshape
		idleCallback $= Just idle
		displayCallback $= display 
		mainLoop

	display :: DisplayCallback
	display = do
		render $ Sphere 1 
		swapBuffers 

	idle :: IdleCallback
	idle = postRedisplay Nothing

	reshape :: ReshapeCallback
	reshape size = do 
		viewport $= (Position 0 0, size)

	@
-}
--
-- You'll need full OpenGL 2.1 support, including GLSL 1.20 to use FreeD. 
--
-- This is a conveniance module, combining all of FreeD's modules.
-----------------------------------------------------------------------------
module Graphics.FreeD (module Export)
where
	import Graphics.FreeD.Data.Drawable as Export
	import Graphics.FreeD.Data.Misc as Export
	import Graphics.FreeD.Rendering.Drawable as Export
	import Graphics.FreeD.Formats.ObjReader as Export
