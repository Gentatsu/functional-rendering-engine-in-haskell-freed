module Display (idle, display) where
 
import Graphics.UI.GLUT hiding (Color, Cube)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD

display :: IORef GLfloat -> IORef (GLdouble, GLdouble) -> DisplayCallback
display angle pos = do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  loadIdentity
  (x',y') <- get pos
  --translate $ Vector3 x' y' 0
  preservingMatrix $ do
    a <- get angle
    let camPos = getCamPos (realToFrac a)
    let eye' = Vertex3 ((fst camPos)*10) 20 ((snd camPos)*10) 
    let tow' = Vertex3 0 0 0
    let up'  = Vector3 0 1 0
    let lightPos = Vertex4 1.0 1.0 1.0 0.0
    let amb' = Color4 0.0 0.0 0.0 1.0
    let dif' = Color4 1.0 1.0 1.0 1.0
    let view = DView {eye=eye', tow=tow', up=up', fov=x', aspect_ratio=1, znear=0.1, zfar=1500}
    let lighting = DLighting {pos=lightPos, amb = amb', dif=dif'}
    let t = picture 4 10.0
    renderDrawable $ Camera view $ Lighting lighting Smooth $ t
  swapBuffers
 
getCamPos :: GLdouble -> (GLdouble,GLdouble)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360


-- The picture is a tree fractal, graded from brown to green
picture :: Int -> GLfloat -> Drawable	
picture degree time
	=  tree degree time (brown)


-- Basic stump shape
stump :: Color -> Drawable
stump color 
	= Color color
	$ Cube 0.1 0.05 0.03 False


-- Make a tree fractal.
tree 	:: Int 		-- Fractal degree
	-> GLfloat	-- time
	-> Color 	-- Color for the stump
	-> Drawable

tree 0 time color = stump color
tree n time color 
 = let	smallTree 
		= Rotate (sin time) 1 0 0
		$ tree (n-1) (- time) color
   in	Drawables
		[ stump color
		, Translate 0 3.0 0 $ smallTree
		, Translate 0 2.4 0 $ Rotate 20 	 1 0 0	 smallTree
		, Translate 0 1.8 0 $ Rotate (-20) 1 0 0 	 smallTree
		, Translate 0 1.2 0 $ Rotate 40 	 1 0 0 	 smallTree
		, Translate 0 0.6 0 $ Rotate (-40) 1 0 0 	 smallTree ]
		

-- A starting colour for the stump
brown :: Color
brown =  makeColorI 139 100 35  255

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
