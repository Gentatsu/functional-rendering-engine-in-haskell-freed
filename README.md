# README #

#Functional Rendering Engine in Haskell#

###Setting up###

In FreeD_2.0/ run 

`cabal configure`

`cabal build`

`cabal install`

This will install the FreeD library to use in your programs. 

This is a MWE of using FreeD, with GLUT:


	import Graphics.UI.GLUT

	import Graphics.FreeD

	

	main :: IO ()

	main = do

		(_progName, _args) <- getArgsAndInitialize

		initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]

		_window <- createWindow ""

		initialWindowSize $= Size 400 400

		reshapeCallback $= Just reshape

		idleCallback $= Just idle

		displayCallback $= display 

		mainLoop



	display :: DisplayCallback

	display = do

		render $ Sphere 1 

		swapBuffers 



	idle :: IdleCallback

	idle = postRedisplay Nothing



	reshape :: ReshapeCallback

	reshape size = do 

		viewport $= (Position 0 0, size)

### Who do I talk to? ###

If you wish to contribute or report any bugs, please email me at: **navs92@hotmail.com**