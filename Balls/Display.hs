module Display (idle, display) where
 
import Graphics.UI.GLUT hiding (Color, Cube, Sphere)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD

data MSphere = MSphere {
	center :: Vector3 GLfloat,
	radius :: GLfloat
}


display :: IORef GLfloat -> IORef (GLdouble, GLdouble) -> DisplayCallback
display angle pos = do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  loadIdentity
  (x',y') <- get pos
  --translate $ Vector3 x' y' 0
  preservingMatrix $ do
    a <- get angle
    let p = points 1
    let d = getSpheres p 
    let camPos = (0.5, 0.5) :: (GLdouble, GLdouble)--getCamPos (realToFrac a)
    let eye' = Vertex3 ((fst camPos)*10) 20 ((snd camPos)*10) 
    let tow' = Vertex3 0 0 0
    let up'  = Vector3 0 1 0
    let lightPos = Vertex4 1.0 1.0 1.0 0.0
    let amb' = Color4 0.0 0.0 0.0 1.0
    let dif' = Color4 1.0 1.0 1.0 1.0
    let view = DView {eye=eye', tow=tow', up=up', fov=x', aspect_ratio=1, znear=0.1, zfar=1500}
    let lighting = DLighting {pos=lightPos, amb = amb', dif=dif'}
    let cube = Color (makeColor 1 1 1 1) $ Cube 1 0.5 0.5 True
    let spheres = Lighting lighting Smooth $ Rotate a 0 0 1 $ d
    let mSpheres = getMSpheres p
    let all = Drawables [spheres, cube]
    renderDrawable $ Camera view $ all 
  swapBuffers
  flush
 
idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
  
getSpheres :: [(GLfloat, GLfloat, GLfloat)] -> Drawable
getSpheres l= Drawables [Translate x y z  $ Color (makeColor x y z 1) (Sphere 0.1) | (x,y,z) <- l]  

getMSpheres :: [(GLfloat, GLfloat, GLfloat)] ->[MSphere]
getMSpheres l = [MSphere{center=Vector3 x y z, radius=0.1} | (x,y,z) <-l]

getCamPos :: GLdouble -> (GLdouble,GLdouble)
getCamPos n = (2 *(sin (a)), 2 * (cos (a)))
					where a = pi * n * 2.0 / 360

