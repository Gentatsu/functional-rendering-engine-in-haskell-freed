module Display (idle, display, initi, State(..), makeState) where
 
import Graphics.UI.GLUT hiding (Color, Cube, lookAt, Texture, ShadingModel, Flat, Sphere)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Data.Text hiding (transpose, map, length, replicate)
import Graphics.GLUtil
import Foreign.Storable (sizeOf)
import Data.Map ((!), keys)
import Linear 

data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

initi :: IORef GLfloat -> IORef (GLfloat, GLfloat) -> IORef [TextureObject] -> State -> IO Drawable
initi angle pos tex state = do  
	objContents <- readFile "f16.obj"
	let 
		eye' = V3 5 10 5
		tow' = V3 0 0 0
		up'  = V3 0 1 0
		{-lightPos = Vertex4 1.0 1.0 1.0 0.0
		amb' = Color4 0.5 0.5 0.5 1.0
		dif' = Color4 0.5 0.5 0.5 1.0
		spec' = Color4 0.0 0.0 0.0 0.0
		view = DView {eye=eye', tow=tow', up=up', fov=x', aspect_ratio=1.0, znear=0.1, zfar=1500}
		lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Phong} 
		d = Texture t2 $ getCubes p a c2
		drawables = Drawables [d, c2]
		mat =  Mat {matAmb= Color4 0.8 0.8 0.8 1, matDif = Color4 0.8 0.8 0.8 1, matSpec = Color4 0.1 0.1 0.1 0.5}-}
		c2 = loadObj $ pack objContents
	cd <- compile c2 
	let drawables = Drawables $ replicate 8 cd
	--let cd = Drawables $ replicate 1024 c2
	--drawables <- compile cd
	t0' <- get (t0 state)
	t <- get elapsedTime
	let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
	putStrLn (show seconds)
	return drawables

display :: IORef GLfloat -> IORef (GLfloat, GLfloat) -> IORef [TextureObject] ->State -> Drawable -> DisplayCallback
display angle posi tex state cd= do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  ;loadIdentity
	;render cd
  ;swapBuffers
  ;	frames state $~! (+1)
	;t0' <- get (t0 state)
	;t <- get elapsedTime
	;when (t - t0' >= 5000) $ do
		;f <- get (frames state)
		;let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		;let fps = fromIntegral f / seconds
		;putStrLn (show seconds)
		;putStrLn (show fps)
		;t0 state $= t
		;frames state $= 0

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
