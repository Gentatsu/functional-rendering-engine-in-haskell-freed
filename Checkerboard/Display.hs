module Display (idle, display, initi, State(..), makeState) where
 
import Graphics.UI.GLUT hiding (Color, Cube, lookAt, Texture, ShadingModel, Flat, Sphere)
import Control.Monad
import Data.IORef
import Points
import Graphics.FreeD
import Data.Text hiding (transpose, map, length, replicate, concat)
import Graphics.GLUtil
import Foreign.Storable (sizeOf)
import Data.Map ((!), keys)
import Linear 

data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

initi :: IORef GLfloat -> IORef (GLfloat, GLfloat) -> IORef [TextureObject] -> State -> IO (Drawable GLfloat)
initi angle pos tex state = do  
	[wood, _] <- get tex
	objContents <- readFile "King.obj"
	cube <- compile $ Cube 1 1 1 :: IO (Drawable GLfloat)	
	let king = loadObj $ pack objContents
	let
		cube = Cube 1 1 1  
		size = 3
		mult = 4
		offset = 2
		black = Color (Color4 0.2 0.2 0.2 1 ) 
		white = Color (Color4 1 1 1 1)
		oddWhiteRow = white $ Drawables $ buildRow size mult 0 cube
		oddBlackRow = black $ Drawables $ buildRow size mult offset cube
		evenWhiteRow = white $ Drawables $ buildRow size mult offset cube
		evenBlackRow = black $ Drawables $ buildRow size mult 0 cube
		oddRow = Drawables [oddWhiteRow, oddBlackRow]
		evenRow = Drawables [evenWhiteRow, evenBlackRow]
		oddCols = Drawables $ buildCol size mult 0 oddRow
		evenCols = Drawables $ buildCol size mult offset evenRow
		lightPos = Vertex4 0 (-10) 20.0 1.0
		amb' = Color4 0.5 0.5 0.5 1.0
		dif' = Color4 0.5 0.5 0.5 1.0
		spec' = Color4 0.5 0.5 0.5 1.0
		lighting = DLighting {pos=lightPos, amb = amb', dif=dif', spec=spec', shadingModel=Phong} 
		player = Translate 0 0 2 $ Texture wood $ cube
		board =  Drawables [oddCols, evenCols, player]
	checkerBoard <- compile $ Lighting lighting board
	--	checkerBoard = board
	t0' <- get (t0 state)
	t <- get elapsedTime
	let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
	putStrLn (show seconds)
	return checkerBoard

--buildRow :: Int -> Int -> Int -> Drawable -> [Drawable] 
--buildRow 0 _ offset d = [Translate (fromIntegral offset) 0 0 d, Translate (fromIntegral (-offset)) 0 0 d]
--buildRow x mult offset d = (Translate (fromIntegral ((x*mult)+offset)) 0 0 d):(Translate (fromIntegral (((-x)*mult)+offset)) 0 0 d):(buildRow (x-1) mult offset d) 

buildRow n mult offset d = [Translate ((x*mult)+offset) 0 0 d |  x <- [(-n)..n]]
buildCol n mult offset d = [Translate 0 ((x*mult)+offset) 0 d |  x <- [(-n)..n]]

--buildCol :: Int -> Int -> Int -> Drawable -> [Drawable] 
--buildCol 0 _ offset d = [Translate 0 (fromIntegral offset) 0 d, Translate 0 (fromIntegral (-offset)) 0 d ]
--buildCol x mult offset d = (Translate 0 (fromIntegral ((x*mult)+offset)) 0 d):(Translate 0 (fromIntegral (((-x)*mult)+offset)) 0 d):(buildCol (x-1) mult offset d)


display :: IORef GLfloat -> IORef (GLfloat, GLfloat) -> IORef [TextureObject] ->State -> Drawable GLfloat -> DisplayCallback
display angle posi tex state cd= do 
  clear [ColorBuffer, DepthBuffer] -- clear depth buffer, too
  ;(x,y) <- get posi
  ;[wood, _] <- get tex
  let
    eye' = V3 0 (-x) 20
    tow' = V3 0 0 0
    up'  = V3 0 1 0
    view = DView {eye=eye', tow=tow', up=up', fov=90, aspectRatio=1.0, znear=0.1, zfar=1500}
    (Drawables [r,c,p]) = cd  
  ;loadIdentity
	;render $ Camera view $ Drawables [r, c, Translate (y*2) 0 0 p]
  ;swapBuffers
  ;	frames state $~! (+1)
	;t0' <- get (t0 state)
	;t <- get elapsedTime
	;when (t - t0' >= 5000) $ do
		;f <- get (frames state)
		;let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		;let fps = fromIntegral f / seconds
		--;putStrLn (show seconds)
		;putStrLn (show fps)
		;t0 state $= t
		;frames state $= 0

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
  d <- get delta
  angle $~! (+ d)
  postRedisplay Nothing
