import Graphics.UI.GLUT
import Data.IORef
import Bindings
import Graphics.GLUtil

main :: IO ()
main = do
	(_progName, _args) <- getArgsAndInitialize
	initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
	_window <- createWindow "Chessboard"
	initialWindowSize $= Size 400 400
	reshapeCallback $= Just reshape
	depthFunc $= Just Less -- the comparison function for depth the buffer
	angle <- newIORef 0
	delta <- newIORef 0.01
	pos <- newIORef (25.0, 0.0)
	t <- loadTex "Wood.jpg"
	t2 <- loadTex "Crate.bmp"
	tex <- newIORef $ [t,t2]
	keyboardMouseCallback $= Just (keyboardMouse delta pos)
	idleCallback $= Just (idle angle delta)
	state <- makeState
	cd <- initi angle pos tex state
	displayCallback $= display angle pos tex state cd
	mainLoop

loadTex :: FilePath -> IO TextureObject
loadTex filename = do
										t <- readTexture filename
										textureWrapMode Texture2D S $= (Repeated, Repeat)
										textureWrapMode Texture2D T $= (Repeated, Repeat)
										textureFilter   Texture2D      $= ((Nearest, Nothing), Nearest) 
										return $ (\(Right x) -> x) t
