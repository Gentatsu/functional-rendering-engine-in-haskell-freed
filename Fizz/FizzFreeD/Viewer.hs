{-# LANGUAGE NoMonomorphismRestriction #-}
{- Viewer.hs: a domain-specific language and opengl viewer for
   multi-variate visualization.

   Authors: Rita Borgo & David Duke, University of Leeds, UK
            Colin Runciman & Malcolm Wallace, University of York UK

   Contact: djd@comp.leeds.ac.uk

   Usage: this code is used as a stand-alone program. You need to set
   up the "main" function toward the end of the file to generate the 
   desired picture, then  compile using ghc, and run the executable.

   A number of example visualizations have been pre-defined at the end
   of this file as simple expressions, each of which demonstrates
   the capabilities of the visualizer.

   See the README file and PictureDSL.hs for further details of the picture language.
-}

module Main where

import qualified Graphics.Rendering.OpenGL as GL
import qualified Graphics.UI.GLUT as GLUT

import Graphics.UI.GLUT hiding (Cube, Color)
import AstroData
import Colour
import Dataset
import Graphics
import Maths
import PictureDSL
import Render hiding (render, display,compile, Camera)
import Graphics.FreeD 
import Data.Foldable hiding (concatMap)
import Linear
import System.Exit ( exitWith, ExitCode(ExitSuccess) )
import Data.IORef
import Control.Monad (when)

-- TODO change this
-- TOP LEVEL: execute an expression specifying a picture.
-- The picture expression is first traversed to determine 
-- the files required; access to these via ByteStrings or
-- streams of Floats are generated via "read_data" and 
-- returned as a "context" - a file environment - that is
-- then used by the picture evaluator.  This two-stage
-- process means that the interpreter does not need to 
-- make IO calls itself, simplifying the presentation.

evalView :: (Dataset d) => View d Float -> IO()
evalView view@(source :> picture)  = 
  do { --GLUT.initialize "Astro Viewer" [] >> return ()
     ; --g <- openGraphics "Scene Viewer" (1000,800)
     ; 	(_progName, _args) <- getArgsAndInitialize
     ; initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
 		 ;initialWindowSize $= Size 1000 800
		 ;_window <- createWindow "Fizz FreeD"
	   ;reshapeCallback $= Just reshape
	   ; 	depthFunc $= Just Less -- the comparison function for depth the buffer	
     ; picture <- evalPicture view
     ; d <- toFreeD picture
     ; let eye' = V3 600 500 600 --eye' = V3 ((fst camPos)*10) 20 ((snd camPos)*10) 
     ; let	tow' = V3 0 0 0
     ; let	up'  = V3 0 1 0
     ; let views = DView {eye=eye', tow=tow', up=up', fov=45, aspectRatio=1.0, znear=0.1, zfar=1500}
     ; let lightPos = Vertex4 1000 500 500 1
     ; let	spec' = Color4 0.3 0.7 0.3 1.0
     ; let mat = Color4 0 0.8 0 1
     ; let lighting = DLighting {pos=lightPos, amb = spec', dif=spec', spec=spec', shadingModel=Gourad} 
     ; --let d = Cube 1 1 1
     ; cd <- compile $ Camera views $ Lighting lighting $ Color mat d
     ; --addScene g $ [Imposter (Group static [axes 600.0 248.0 248.0, picture]) (bbox 600 248 248) ]
     ; state <- makeState
     ; GLUT.displayCallback GLUT.$= display cd state
     ; 	idleCallback $= Just idle
     ; GLUT.mainLoop
     }

data State = State {
   frames  :: IORef Int,
   t0      :: IORef Int
   }

makeState :: IO State
makeState = do
   f <- newIORef 0
   t <- newIORef 0
   return $ State { frames = f, t0 = t}

reshape :: ReshapeCallback
reshape size = do 
  viewport $= (Position 0 0, size)

idle :: IdleCallback
idle  = do
  postRedisplay Nothing

display :: Drawable -> State -> GLUT.DisplayCallback
display cd state= do 
  GLUT.clear [GLUT.ColorBuffer, GLUT.DepthBuffer] -- clear depth buffer, too
  GLUT.loadIdentity
  render cd
  GLUT.swapBuffers
  exitWith ExitSuccess
  ;	frames state $~! (+1)
	;t0' <- get (t0 state)
	;t <- get elapsedTime
	;when (t - t0' >= 5000) $ do
		;f <- get (frames state)
		;let seconds = fromIntegral (t - t0') / 1000 :: GLfloat
		;let fps = fromIntegral f / seconds
		;putStrLn (show seconds)
		;putStrLn (show fps)
		;t0 state $= t
		;frames state $= 0
  
toFreeD :: HsScene -> IO Drawable
toFreeD (Geometry _ pMode geom) = do
	d <- mapM fromGeometry geom
	return $ Drawables d
toFreeD (Group _ g) = do
	d <- mapM toFreeD g
	return $ Drawables $ d
toFreeD _ = return Blank

fromGeometry :: HsGeom -> IO Drawable
fromGeometry (HsGeomNt hs) = do
	let d = DObject (DGeometryNt hs) GLUT.Triangles
	return d  
fromGeometry _ = do
	return Blank


main :: IO ()
main = do { evalView $ surface }

{- The remainder of this file contains examples of picture-generating 
   expressions.  These can either be entered into the ghc command line,
   or inserted into the "main" function as above and then compiled.
-}

surface = (from4 35 G) :> 
          (Surface red (Single 2500))

surface2 = (from4 30 Mv) :> 
          (Surface red (Single 2500))

surfaceFull = (fromFull 60 G) :> 
          (Surface red (Single 2500))          

draw = (from4 35 G) :>
       Draw [ (Surface red (Single 2500))
            , (Surface blue (Single 10000))
            , (Surface green (Single 15000))
            ]

surfaceAnim = (from4 35 G) :>
              Anim [Surface green (Single t) | t <- [0, 3000 .. 20000]]

anim = (from4 35 G) :>
       Anim [ (Surface red (Single 2500))
            , (Surface blue (Single 10000))
            , (Surface green (Single 15000))
            ]

volume = (from4 35 G) :>
         (Volume vis5D)

sliced = (VisData (Range 0 599) (Range 0 247) (Single 124) 15 G) :>
         (Slice reds)

contour = (VisData (Range 0 599) (Range 0 247) (Single 124) 15 G) :> 
          (Contour greens (Sampled 1 500 10001))

contourAnim = (VisData (Range 0 599) (Range 0 247) (Single 124) 15 G) :>
              Anim [Contour green (Single t) | t <- [0, 500 .. 20000]]
