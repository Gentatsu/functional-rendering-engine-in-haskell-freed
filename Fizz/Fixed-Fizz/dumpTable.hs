
module Main where

import McLookupTable
import RectGrid
import CellTypes
import System.IO



main = do h <- openFile "table.txt" WriteMode
          mapM_ (dump h) [Cell8 False False False False False False False False .. Cell8 True True True True True True True True]
          hClose h

dump fh idx
    = do let es = mcLookup idx
         let (Cell8 a b c d e f g h) = idx
         dumpKey fh [a,b,c,d,e,f,g,h]
         hPutStr fh "\t"
         dumpEntry fh es

dumpKey h flags 
    = mapM_ showFlag $ zip flags [0..7]
      where
          showFlag (False,_) = return ()
          showFlag (True, i) = hPutStr h (show i)

dumpEntry h [] = hPutStr h "\n"
dumpEntry h (u:v:w:xs) = do dumpEdge u
                            dumpEdge v
                            dumpEdge w
                            hPutStr h " "
                            dumpEntry h xs
                         where
                            dumpEdge (x,y) = do hPutStr h (show $ fromEnum x)
                                                hPutStr h (show $ fromEnum y)


