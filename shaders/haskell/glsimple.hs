import Graphics.UI.GLUT
import Graphics.Rendering.OpenGL.GL.Shaders.Uniform
import Graphics.GLUtil
import Foreign.Storable (sizeOf)
import Control.Concurrent (threadDelay)
import Control.Applicative
import Data.Map ((!), keys)
import Data.Word
import Data.Vect.Floating.Base
import Graphics.Rendering.OpenGL.Raw.Core31
import Graphics.Rendering.OpenGL
import Data.IORef
import Linear hiding (rotate)

main :: IO ()
main = do
	(_progName, _args) <- getArgsAndInitialize
	_window <- createWindow "Interpolation Example"
	vbo <- makeBuffer ArrayBuffer vertices
	let vao = bindVertexArrayObject
	ebo <- makeBuffer ElementArrayBuffer elements
	p <- simpleShaderProgram "shad.ver" "shad.frag"
	currentProgram $= Just (program p)
	let posn = fst $ attribs p ! "position"
	let col = fst $ attribs p ! "color"
	let stride = fromIntegral $ sizeOf (undefined::GLfloat) * 5
	let offset = fromIntegral $ sizeOf (undefined::GLfloat) * 2
	let posVad = VertexArrayDescriptor 2 Float stride offset0
	let colVad = VertexArrayDescriptor 3 Float stride (offsetPtr offset)
	vertexAttribPointer posn $= (ToFloat, posVad)
	vertexAttribArray posn   $= Enabled
	vertexAttribPointer col $= (ToFloat, colVad)
	vertexAttribArray col  $= Enabled
	angle <- newIORef 0
	delta <- newIORef 0.05
	idleCallback $= Just (idle angle delta)
	displayCallback $= display angle p 
	mainLoop
 
display :: IORef GLfloat -> ShaderProgram -> DisplayCallback
display angle p = do
	clear [ColorBuffer]
	preservingMatrix $ do
		a <- get angle
		--rotate a $ Vector3 0 1 0
		mvMatrix <- get ((matrix $ Just $ Modelview 0)::StateVar(GLmatrix GLfloat))
		pMatrix <- get ((matrix $ Just Projection)::StateVar(GLmatrix GLfloat))
		mv <- getMatrixComponents ColumnMajor mvMatrix
		pm <- 	getMatrixComponents ColumnMajor pMatrix
		let mvm = getMat mv
		let pM = getMat pm 
		let mvmp = mvm !*! pM
		setUniform p "uModelViewMatrix" mvmp
	drawElements Triangles 6 UnsignedInt offset0
	swapBuffers


getMat :: [GLfloat] -> M44 GLfloat
getMat (a11:a12:a13:a14: 
				a21:a22:a23:a24:
				a31:a32:a33:a34:
				a41:a42:a43:a44:_) = V4 (V4 a11 a12 a13 a14)
                 (V4 a21 a22 a23 a24)
                 (V4 a31 a32 a33 a34)
                 (V4 a41 a42 a43 a44)
                 
vertices :: [GLfloat]
vertices = [-0.5, 0.5, 1, 0, 0,
			0.5, 0.5, 0, 1, 0, 
			0.5, -0.5, 0, 0, 1,
			-0.5, -0.5, 1, 1, 1]
			
elements :: [GLuint]
elements = [0, 1, 2,
			2, 3, 0]

idle :: IORef GLfloat -> IORef GLfloat -> IdleCallback
idle angle delta = do
	d <- get delta
	angle $~! (+ d)
	postRedisplay Nothing
